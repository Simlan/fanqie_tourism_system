/** By chaituan@126.com */
layui.form.verify({
  username: function(value, item){ //value：表单的值、item：表单的DOM对象
    if(!new RegExp("^[a-zA-Z0-9_\u4e00-\u9fa5\\s·]+$").test(value)){
      return '用户名不能有特殊字符';
    }
    if(/(^\_)|(\__)|(\_+$)/.test(value)){
      return '用户名首尾不能出现下划线\'_\'';
    }
    if(/^\d+\d+\d$/.test(value)){
      return '用户名不能全为数字';
    }
  },
  thumb:function(value,item){
	  if(!value){
		  $(item).prev().prev().prev().prev().css({"color":"#FF5722","border":"1px solid"});
		  return '请上传文件';
	  }
  }
  //我们既支持上述函数式的方式，也支持下述数组的形式
  //数组的两个值分别代表：[正则匹配、匹配不符时的提示文字]
  ,pass: [/^[\S]{6,12}$/,'密码必须6到12位，且不能出现空格'],
  repass:function(value){
	  var p = $('#pwd').val();
	  if(p!=value){
		  return '两次输入的密码不一样';
	  }
  }
  
});

//提交事件
layui.form.on('submit(sub)',function(data){
	new f_ajax(data);
	return false;
});
//数据数据 异步提交查询
layui.form.on('submit(table-find)',function(){
	layui.table.reload('common',{//这里的find 是为了后台数据处理
		where:{name:$('#table-find-val').val(),find:'find',total:''},
		done:function(res, curr, count){
			this.where.total = count;
			this.where.find = '';
			layer.photos({photos:'.img_view'});//添加预览
		}
	});
	return false;
});

//批量删除---------------------------------------------
layui.form.on('submit(dels)',function(data){
	var $checkbox = $('tbody input[type="checkbox"][ckd="ckd"]');
	if($checkbox.is(":checked")){
		new f_ajax(data);
	}else{
		layer.msg("请选择您要删除的数据");
	}
	return false;
});
//批量删除后执行方法
function dels_row(data){
	if(data.state==1){
		var d = data.data;
		for(var i=0;i<d.length;i++){
			$("#list_"+d[i]).remove();
		}
		layer.msg(data.message,{icon: data.state});
	}else{
		layer.msg(data.message,{icon: data.state});
	}
}
//单个删除
$('.f_del').click(function(e){
	e.preventDefault();
	var url = $(this).data('href');
	var self = this;
	layer.confirm('确定要执行当前操作？', {btn: ['是','否']}, function(index){
			layer.close(index);
			var l = loads();
			$.get(url, function(data) {
                if ( data.state == 1 ) {
                    $(self).parents('tr').remove();
                }
                layer.msg(data.message,{icon: data.state});
                layer.close(l);
            }, 'json');
		});
	return false;
});
//开关 主要用于列表页面的  开启和关闭
layui.form.on('switch(open)', function(data){
	var url = data.elem.dataset.url;
	var l = loads();
	var open = data.elem.checked?1:0;//选中是1 取消是0
	$.get(url,{open:open},function(data) {
        if (data.state == 1) {
        	layer.msg(data.message,{icon: data.state});
        }else{
        	layer.msg(data.message,{icon: data.state});
        }
        layer.close(l);
    }, 'json');
	return false;
})

//全选 table 静态的全选
layui.form.on('checkbox(allChoose)', function(data){
	var child = $(data.elem).parents('table').find('tbody input[type="checkbox"][ckd="ckd"]:not([name="show"])');
	child.each(function(index, item){
		item.checked = data.elem.checked;
	});
	layui.form.render('checkbox');
});


//自动计算数据列表表格的colspan
$('.empty-table-td').each(function() {
    var th = $(this).parent().parent().prev().children(":first").children();
    $(this).attr('colspan', th.length);
    $(this).css('text-align', 'center');
});

/*****动态表格事件处理****/
layui.table.on('tool(common)',function(obj){
	var data = obj.data;
	if(obj.event == 'del'){
		var url = $(event.target).data('href');
		layer.confirm('确定要执行当前操作？', {btn: ['是','否']}, function(index){
				layer.close(index);
				var l = loads();
				$.get(url, function(data) {
	                if (data.state == 1) {
	                	obj.del();
	                }
	                layer.msg(data.message,{icon: data.state});
	                layer.close(l);
	            }, 'json');
			});
	}
});

/********批量删除****无重载删除表内数据，因为lay没有提供del方法 ，自己写** 第一步通过table监听，如果全选则全部删除，如果不是则放入数组
 * obj.checked 当前是否选中状态 true false  obj.data 选中行的相关数据  obj.type 如果触发的是全选，则为：all，如果触发的是单选，则为：one
 * **/
var tabArr = new Object();
layui.table.on('checkbox(common)', function(obj){
	if(obj.type=='all')return ;
	if (obj.checked){//把选中的放入数组
		tabArr[obj.data.id] = obj.data.LAY_TABLE_INDEX;
	}else{//取消选中的 移除数据
		delete tabArr[obj.data.id];
	}
});
/**
 * checkStatus.data) //获取选中行的数据  checkStatus.data.length) //获取选中行数量，可作为是否有选中行的条件 checkStatus.isAll ) //表格是否全选
 */
$('.f_dels').click(function(){
	var url = $(this).attr('url');
	var checkStatus = layui.table.checkStatus('common'),data = checkStatus.data, tmpArr = new Object(), ids = new Object();
	for(var i = 0; i< data.length; i++){
		if(!tabArr){
			ids[i] = data[i].id;	
		}else{
			tmpArr[data[i].id] = tabArr[data[i].id];
			ids[i] = data[i].id;	
		}
	}
	if(data.length){
		layer.confirm('确定要执行当前操作？', {btn: ['是','否']}, function(index){
			layer.close(index);
			var l = loads();
			$.post(url,{checked:ids},function(d) {
                if (d.state == 1) {
                	if(checkStatus.isAll){
                		$("tr").remove();
                	}else{
                		$.each(tmpArr, function(k){
                			$("tr[data-index="+tmpArr[k]+"]").remove();
                		});
                	}
                }
                layer.msg(d.message,{icon: d.state});
                layer.close(l);
            },'json');
		});
	}else{
		layer.msg("请选择您要删除的数据");
	}
});


document.oncontextmenu = function () {  
    return false;  
}

//document.onkeydown = function () {  
//    if (window.event && window.event.keyCode == 123) {  
//        window.event.returnValue = false;  
//    } 
//}  

//加载中
function loads(){
	return layer.load(1, {shade: [0.5,'#000']});
}