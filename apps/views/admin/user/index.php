<?php echo template('admin/header');echo template('admin/sider');?>

<div class="layui-body">
	<div class="childrenBody childrenBody_show">
		<blockquote class="layui-elem-quote news_search">
				<div class="layui-inline">
					<form class="layui-form">
					<div class="layui-input-inline">
						<input type="text"  id="table-find-val" placeholder="请输入姓名或者帐号" class="layui-input" lay-verify='required'>
					</div>
				    <?php echo admin_btn('', 'find',"",'lay-filter="table-find"')?>
					</form>
				</div>
				<div class="layui-inline">
					<?php echo admin_btn(site_url("$dr_url/fp"),'btn','gxfp','lay-submit','勾选后分配');?>
				</div>
				<div class="layui-inline">
					<?php echo admin_btn(site_url("$dr_url/dels"),'dels','layui-btn-danger f_dels');?>
				</div>
		</blockquote>
		<table  id="user" lay-filter="common" ></table>
	</div>
</div>
<div id="fx"  style="display: none;">
	<form class="layui-form">
		<select name="fxxz">
			<?php if($fx){foreach ($fx as $v){?>
				<option value="<?php echo $v['id'];?>"><?php echo $v['nickname'];?></option>
			<?php }}else{ ?>
				<option value="0">没有数据</option>
			<?php }?>
		</select>
	</form>
</div>
<?php echo template('admin/script');?>
<script type="text/html" id="operation">
<?php echo admin_btn(site_url($dr_url.'/del/id-{{d.id}}'),'del','layui-btn-xs f_del_d','lay-event="del"');?>
</script>
<script type="text/html" id="sth">
<input type="checkbox" lay-text='正常|锁定' lay-skin="switch" lay-filter='open' {{# if(d.state==1){ }} checked {{#  } }}   data-url="<?php echo site_url($dr_url.'/lock/id-{{d.id}}')?>" >
</script>
<script>
//执行渲染
var tab = layui.table.render({
	elem: '#user', //指定原始表格元素选择器（推荐id选择器）
	id:'common',//给事件用的
	height: 'full-250', //容器高度
	url:'<?php echo site_url("$dr_url/lists")?>',
	cols: [[
	       {checkbox: true},
	       {field: 'id', title: 'ID', width: 80,sort:true},
	       {field: 'nickname', title: '昵称'},
	       {field:'thumb',title:'头像',toolbar:'<div><div class="img_view"><img src="{{d.thumb}}"></div></div>'},
	       {field: 'p_1', title: '上级ID'},
	       {field: 'addtime', title: '注册时间',toolbar:'<div>{{Time(d.addtime, "%y-%M-%d %h:%m:%s")}}</div>'},
	       {field: 'province', title: '省'},
	       {field: 'city', title: '市'},
	       {field: 'district', title: '区'},
	       {field: 'state', title: '锁定',toolbar: '#sth',width: 90},
	       {field: 'right', title: '操作',toolbar: '#operation',width: 150}
	       ]],
	limit: 15,
	page:true,
	response:{msgName:'message'},
	done:function(res, curr, count){
		this.where.total = count;
		layer.photos({photos:'.img_view'});//添加预览
	}
});

$('.gxfp').click(function(){
	var url = $(this).attr('url');
	var checkStatus = layui.table.checkStatus('common'),data = checkStatus.data, tmpArr = new Object(), ids = new Object();
	for(var i = 0; i< data.length; i++){
		if(!tabArr){
			ids[i] = data[i].id;	
		}else{
			tmpArr[data[i].id] = tabArr[data[i].id];
			ids[i] = data[i].id;	
		}
	}
	layer.open({
		area: ['20%', '40%'],
		title:false,
		content:$('#fx').html(),
		btn:['分配','取消'],
		btnAlign:'c',
		yes:function(){
			var fx_id = $('.layui-layer-content select[name="fxxz"]').val();
			if(fx_id==0){
				layer.msg("请选择分销商");
				return false;
			}
			if(data.length){
				layer.confirm('确定要执行当前操作？', {btn: ['是','否']}, function(index){
					layer.close(index);
					var l = loads();
					$.post(url,{ids:ids,pid:fx_id},function(d) {
		                layer.msg(d.message,{icon: d.state});
		                layer.close(l);
		            },'json');
				});
			}else{
				layer.msg("请选择您要分配的数据");
			}
		},
		success:function(){
			layui.form.render('select');
		}
	});
});

$('#fp').click(function(){
	
	
});
</script>
<?php echo template('admin/footer');?>