<?php echo template('admin/header');echo template('admin/sider');?>
<div class="layui-body">
	<div class="childrenBody childrenBody_show">
			<!-- content form start -->
			<form class="layui-form" method="post">
				<div class="layui-tab layui-tab-card">
					<ul class="layui-tab-title">
	                    <?php foreach ($menuGroups as $key=>$val){?>
	                    	<li class="<?php if($key=='system')echo 'layui-this';?>">
	                    		<?php echo $val['name'];?>
	                    	</li>
	                    <?php }?>
	                </ul>
					<div class="layui-tab-content">
	                    <?php foreach ($menuGroups as $key=>$gval){?>
	                    <div class="layui-tab-item <?php if($key=='system')echo 'layui-show';?>" >
							<table class="layui-table ">
								<thead>
									<tr>
										<th>ID</th>
										<th>菜单名称</th>
										<th>URL</th>
										<th>排序数字</th>
										<th>是否显示</th>
										<th>添加时间</th>
										<th>操作</th>
									</tr>
								</thead>
								<tbody>
	                            <?php if (empty($items[$key])){?>
	                            <tr>
									<td class="empty-table-td"><?php echo $emptyRecord;?></td>
								</tr>
	                            <?php }?>
								<?php foreach ($items[$key] as $value){ ?>
	                            <tr id="items_<?php echo $value['id']; ?>">
										<td>
	                                   <?php echo $value['id']; ?>
	                                    <input type="hidden" name="hids[<?php echo $value['id']; ?>]" value="<?php echo $value['id']; ?>" />
										</td>
										<td><input type="text" name="data[<?php echo $value['id']; ?>][name]" value="<?php echo $value['name']; ?>" class="layui-input" /></td>

										<td><input type="text" name="data[<?php echo $value['id']; ?>][url]" value="<?php echo $value['url']; ?>" class="layui-input" /></td>
										<td><input type="text" name="data[<?php echo $value['id']; ?>][sort_num]" value="<?php echo $value['sort_num']; ?>" class="layui-input" /></td>
										<td><input type="checkbox" name="data[<?php echo $value['id']; ?>][ishow]" value="1" lay-skin="switch" lay-text="是|否" <?php if($value['ishow']==1){ echo 'checked';}?>></td>
										<td><?php echo date('Y-m-d',$value['add_time']); ?></td>
										<td>
										<div class="layui-btn-group">
										  <?php echo admin_btn(site_url($dr_url.'/add/pid-'.$value['id']),'add','layui-btn-xs');?>
										  <?php echo admin_btn(site_url($dr_url.'/edit/id-'.$value['id']),'edit','layui-btn-xs');?>
										  <?php echo admin_btn(site_url($dr_url.'/del/id-'.$value['id']),'del','layui-btn-xs f_del');?>
										</div>
										</td>
									</tr>
									
									<!-- 二级菜单 -->
	                            <?php foreach ($subitems[$value['id']] as $val){?>
	                            <tr id="items_<?php echo $val['id']; ?>" class="items_<?php echo $val['id']; ?>">
										<td>
	                                    <?php echo $val['id']; ?>
	                                    <input type="hidden" name="hids[<?php echo $val['id']; ?>]" value="<?php echo $val['id']; ?>" />
										</td>
										<td style="padding-left: 30px;">
										<input type="hidden" name="hids[<?php echo $val['id']; ?>]" value="<?php echo $val['id']; ?>" /> 
										<input type="text" name="data[<?php echo $val['id']; ?>][name]" value="<?php echo $val['name']; ?>" class="layui-input" />
										</td>
										<td><input type="text" name="data[<?php echo $val['id']; ?>][url]" value="<?php echo $val['url']; ?>" class="layui-input" /></td>
										<td><input type="text" name="data[<?php echo $val['id']; ?>][sort_num]" value="<?php echo $val['sort_num']; ?>" class="layui-input" /></td>
										<td><input type="checkbox" name="data[<?php echo $val['id']; ?>][ishow]" value="1" lay-text="是|否" lay-skin="switch" <?php if($val['ishow']==1){ echo 'checked';}?>></td>
										<td><?php echo date('Y-m-d',$val['add_time']); ?></td>
										<td>
										<div class="layui-btn-group">
										  <?php echo admin_btn(site_url($dr_url.'/edit/id-'.$val['id']),'edit','layui-btn-xs');?>
										  <?php echo admin_btn(site_url($dr_url.'/del/id-'.$val['id']),'del','layui-btn-xs f_del');?>
										</div>
										</td>
									</tr>
	                            <?php }}?>
	                            </tbody>
							</table>
							<footer class="panel-footer">
								<div class="layui-row">
								    <div class="layui-col-xs12">
								    	<div class="layui-inline">
											<?php echo admin_btn(site_url($dr_url.'/quicksave'), 'save','',"lay-filter='sub' location=''");?>
										</div>
										<div class="layui-inline">
											<?php echo admin_btn(site_url($dr_url.'/add'), 'add','layui-btn-normal');?>
										</div>
								    </div>
								</div>
							</footer>
						</div>
	                    <?php }?>
					</div>
				</div>
			</form>
			<!-- form END -->
	</div>
</div>
<?php echo template('admin/script');?>
<?php echo template('admin/footer');?>