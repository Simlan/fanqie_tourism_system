<?php echo template('admin/header');echo template('admin/sider');?>
<div class="layui-body">
	<div class="childrenBody childrenBody_show">
		<blockquote class="layui-elem-quote a-e-quote">
				<div class="layui-inline">添加权限</div>
				<div class="layui-inline f-right"><?php echo admin_btn($index_url, '', 'layui-btn-xs','','返回')?></div>
		</blockquote>
		<form class="layui-form a-e-form" method="post">
				<?php foreach ($items as $k=>$v){?>
				<div class="layui-form-item ">
					<label class="layui-form-label"><?php echo $v['name'];?></label>
					<div class="layui-input-block">
					<input type="checkbox"  title="全选" lay-filter='per_all'>
					<?php  foreach ($v['methods'] as $ks=>$vs){?>
						<input type="checkbox" name="<?php echo $k.'@'.$ks;?>" <?php if(isset($permissions[$k.'@'.$ks])==1)echo 'checked';?>   title="<?php echo $vs;?>" value="1">
					<?php }?>
					</div>
				</div>
				<?php }?>
				<div class="layui-form-item">
					<div class="layui-input-block">
						<input type="hidden" name="id" value="<?php echo $item['id'];?>" /> 
						<?php echo admin_btn(site_url('adminct/adminrole/permission'),'save','layui-btn-lg',"lay-filter='sub' location='$index_url'")?>
					</div>
				</div>
		</form>
	</div>
</div>
					
<?php echo template('admin/script');?>
<script type="text/javascript">
$(function(){
	layui.form.on('checkbox(per_all)', function(data){
		var child = $(data.elem).nextAll('input[type="checkbox"]:not([name="show"])');
		child.each(function(index, item){
			item.checked = data.elem.checked;
		});
		layui.form.render('checkbox');
	});
});

</script>
<?php echo template('admin/footer');?>