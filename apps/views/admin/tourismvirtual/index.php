<?php echo template('admin/header');echo template('admin/sider');?>
<div class="layui-body">
	<div class="childrenBody childrenBody_show">
		<blockquote class="layui-elem-quote">
			<div class="layui-inline">
					<?php echo admin_btn(site_url($add_url.'/tcid-'.$tcid),'add','layui-btn-normal');?>
			</div>
		</blockquote>
		<form class="layui-form" method="post">
		  	<table class="layui-table">
			    <thead>
					<tr>
						<th>ID</th>
						<th>姓名</th>
						<th>照片</th>
						<th>人数</th>
						<th>操作</th>
					</tr> 
			    </thead>
			    <tbody>
                    <?php if (empty($items)){?>
                    <tr>
						<td class="empty-table-td"><?php echo $emptyRecord;?></td>
					</tr>
                    <?php }?>
					<?php foreach ($items as $k=>$v){ ?>
                     <tr id="list_<?php echo $v['id']?>">
						<td><?php echo $v['id']; ?></td>
						<td><?php echo $v['nickname']; ?></td>
						<td><div class="img_view"><img src="<?php echo $v['thumb']; ?>"></div></td>
						<td><?php echo $v['num']; ?></td>
						<td>
						  <?php echo admin_btn(site_url($dr_url.'/del/id-'.$v['id']),'del','layui-btn-xs f_del');?>
						</td>
					</tr>
                    <?php }?>
	             </tbody>
			</table>
		</form>
	</div>
</div>
<?php echo template('admin/script');?>
<script>
layer.photos({photos:'.img_view'});//添加预览
</script>
<?php echo template('admin/footer');?>