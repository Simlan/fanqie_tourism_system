<?php $data['definedcss'] = array(CSS_PATH.'menu/menu'); echo template('admin/header',$data);echo template('admin/sider'); ?>
<!-- content -->

<script src="<?php echo JS_PATH.'/menu/modernizr.js'?>"></script>

<script src="<?php echo JS_PATH.'/menu/jquery.js'?>"></script>

<div class="layui-body">
	<div class="childrenBody childrenBody_show">
		<div class="layui-row">
				<blockquote class="layui-elem-quote a-e-quote">
				<div class="layui-inline">菜单管理</div>
				</blockquote>
				<div class="panels panel-success" id="divMain">
					<div class="panels-body" data-bind="with:Menus" id="divMenu" style="display: none;">
                        <div class="layui-row" data-bind="foreach:newArray(3)">
                            <div class="list-group layui-col-md4 clearFill bn">
                                <!--ko if:($parent.button.length>0 && $parent.button[$index()]!=undefined && $parent.button[$index()].sub_button!=undefined ) -->
                                <!--ko foreach:newArray((4-$parent.button[$index()].sub_button.length)) -->
                                <div class="list-group-item bn"></div>
                                <!--/ko-->
                                <!--ko if:$parent.button[$index()].sub_button.length<5 -->
                                <div class="list-group-item" data-bind="click:function (){$root.AddMenu($index())}">
                                    <i class="fa fa-plus"></i>
                                </div>
                                <!--/ko-->
                                <!--ko foreach:($parent.button[$index()].sub_button) -->
                                <div class="list-group-item" data-bind="text:name,attr:{'bottonIndex':$parent.value,'subbottonIndex':$index()},click:function (){$root.EditMenu($data,$parent.value,$index())}"></div>
                                <!--/ko-->
                                <!--/ko -->
                                <!--ko if: $parent.button[$index()]!=undefined && $parent.button[$index()].sub_button==undefined -->
                                <div class="list-group-item bn"></div>
                                <div class="list-group-item bn"></div>
                                <div class="list-group-item bn"></div>
                                <div class="list-group-item bn"></div>
                                <div class="list-group-item" data-bind="click:function (){$root.AddMenu($index())}">
                                    <i class="fa fa-plus"></i>
                                </div>
                                <!--/ko-->
                                <!--ko if: $parent.button[$index()]==undefined -->
                                <div class="list-group-item bn"></div>
                                <div class="list-group-item bn"></div>
                                <div class="list-group-item bn"></div>
                                <div class="list-group-item bn"></div>
                                <div class="list-group-item bn"></div>
                                <!--/ko-->
                            </div>
                        </div>
                        <!--ko foreach:button -->
                        <div class="layui-col-md4">
                        	<div class="list-group-item list-group-item-danger" data-bind="text:name,attr:{'bottonindex':$index()},click:function (){$root.EditMenu($data,$index(),-1)}"></div>
                        </div>
                        <!--/ko-->
                        <!--ko if:button.length < 3 -->
                        <div class="layui-col-md4">
	                        <div class="list-group-item" data-bind="click:function (){$root.AddMenu();}">
	                            <i class="fa fa-plus"></i>
	                        </div>
                        </div>
                        <!--/ko-->
                        <div class="layui-row" >
	                        <div class="layui-col-md12" style="border: 1px solid #EEEEEE; padding: 15px; margin-top: 15px;" data-bind="with:$root.Menu,visible:($root.Menu()!=undefined)">
	                            <form class="layui-form" id="MenuForm" onsubmit="return false;">
	                           		<div class="layui-form-item">
	                           			<div class="layui-inline">
			                                    <input type="text" class="layui-input" name="name"  data-placement="top" data-toggle="popover"  placeholder="请输入名称" data-bind="value:name,event:{'keyup':$root.EventNameErrorMessage},attr:{'data-content':$root.NameErrorMessage}">
		                                </div>
	                                	<div class="layui-inline" >
		                                    <select lay-ignore class="layui-input" onchange="$('#txtMenuButtonValue').attr('placeholder', $(this).find('option:selected').attr('pl'))" data-bind="value:type">
	                                    		<option value="view" pl="请输入Url">跳转URL</option>
		                                        <option value="click" pl="请输入Key">点击推事件</option>
		                                        <option value="scancode_push" pl="请输入Key">扫码推事件</option>
		                                        <option value="scancode_waitmsg" pl="请输入Key">扫码推事件且弹出“消息接收中”提示框</option>
		                                        <option value="pic_sysphoto" pl="请输入Key">弹出系统拍照发图</option>
		                                        <option value="pic_photo_or_album" pl="请输入Key">弹出拍照或者相册发图</option>
		                                        <option value="pic_weixin" pl="请输入Key"> 弹出微信相册发图器</option>
		                                        <option value="location_select" pl="请输入Key">弹出地理位置选择器</option>
		                                    </select>
	                                    </div>
	                                </div>
	                                <div class="layui-form-item">
	                                    <input type="text" id="txtMenuButtonValue" name="value" class="layui-input" placeholder="请输入Url" data-placement="top" data-toggle="popover" data-bind="value:value,event:{'keyup':$root.EventValueErrorMessage},attr:{'data-content':$root.ValueErrorMessage}">
	                                </div>
	                                <div class="form-group layui-col-md12">
	                                    <button type="submit" class="layui-btn layui-btn-sm " data-bind="click:$root.MenuSave">确定</button>
	                                    <button type="submit" class="layui-btn layui-btn-sm layui-btn-danger" data-bind="visible:$root.isEditMenu,click:$root.DeleteMenu">删除</button>
	                                    <button type="button" class="layui-btn layui-btn-sm layui-btn-primary" title="上移" data-bind="visible:$root.isEditMenu(),disable:!$root.IsUp(),click:$root.Up"><i class="fa fa-chevron-circle-up" aria-hidden="true"></i></button>
	                                    <button type="button" class="layui-btn layui-btn-sm layui-btn-primary" title="下移" data-bind="visible:$root.isEditMenu(),disable:!$root.IsDown(),click:$root.Down"><i class="fa fa-chevron-circle-down" aria-hidden="true"></i></button>
	                                    <button type="button" class="layui-btn layui-btn-sm layui-btn-primary" title="左移" data-bind="visible:$root.isEditMenu(),disable:!$root.IsLeft(),click:$root.Left"><i class="fa fa-chevron-circle-left" aria-hidden="true"></i></button>
	                                    <button type="button" class="layui-btn layui-btn-sm layui-btn-primary" title="右移" data-bind="visible:$root.isEditMenu(),disable:!$root.IsRight(),click:$root.Right"><i class="fa fa-chevron-circle-right" aria-hidden="true"></i></button>
	                                    <button type="button" class="layui-btn layui-btn-sm layui-btn-primary" title="复制菜单" data-bind="visible:$root.isEditMenu(),click:$root.Copy">复制</button>
	                                    <button type="button" class="layui-btn layui-btn-sm layui-btn-primary" title="粘贴菜单" data-bind="click:$root.Paste">粘贴</button>
	                                    <button type="submit" class="layui-btn layui-btn-sm layui-btn-primary" data-bind="click:$root.CancelMenuSave">关闭</button>
	                                </div>
	                            </form>
	                        </div>
                        </div>
                    </div>
                    <div class="panels-footer" data-bind="with:Menus">
                        <button id="btnSubmitMenu" type="button" class="layui-btn" data-bind="click:$root.SaveMenus" data-placement="top" title="发布到微信"><i class="fa fa-upload" aria-hidden="true"></i> 保存并发布</button>
                        <button id="btnQueryMenu" type="button" class="layui-btn layui-btn-primary" data-bind="click:function (){$root.EditMenus(true)}"  data-placement="top" title="查询微信现有菜单"><i class="fa fa-download" aria-hidden="true"></i> 拉取菜单（从后台创建的才能拉取到）</button>
                    </div>
                </div>
			</div>
			
			<div class="layui-row">
				<blockquote class="layui-elem-quote a-e-quote">
				<div class="layui-inline">实时JSON（技术使用）</div>
				</blockquote>
				<div class="panels panels-default">
	                    <div class="panels-body">
	                    	<pre id="jsonShow" style="padding:0;border:none;background-color:#fff;" data-bind="html:JSON.stringify($root.NewMenu(),null,4)"></pre>
	                	</div>
				</div>
			</div>
			
	</div>
</div>
<script src="<?php echo JS_PATH.'menu/bootstrap.min.js'?>"></script>
<script src="<?php echo JS_PATH.'menu/jquery.validate.js'?>"></script>
<script src="<?php echo JS_PATH.'menu/jquery.validate.unobtrusive.js'?>"></script>
<script src="<?php echo JS_PATH.'menu/menu.js'?>"></script>
<script type="text/javascript" src="<?php echo LAYUI.'layui.all.js'?>" ></script>

<script type="text/javascript" src="<?php echo LAYUI.'form_verify.js'?>" ></script>
<script type="text/javascript" src="<?php echo JS_PATH.'f_ajax.js'?>" ></script>
<script type="text/javascript" src="<?php echo JS_PATH.'icon.js'?>" ></script>

</div>
</body>
</html>