<?php $data['definedcss'] = array(PLUGIN.'umeditor/themes/default/css/umeditor.min');echo template('admin/header',$data);echo template('admin/sider');?>
<div class="layui-body">
	<div class="childrenBody childrenBody_show">
		<blockquote class="layui-elem-quote a-e-quote">
				<div class="layui-inline">编辑</div>
				<div class="layui-inline f-right"><?php echo admin_btn($index_url, '', 'layui-btn-xs','','返回')?></div>
		</blockquote>
				<form class="layui-form  a-e-form" method="post">
						<div class="layui-tab layui-tab-brief" >
						  <ul class="layui-tab-title">
						    <li class="layui-this">基础设置</li>
						    <li>活动内容</li>
						    <li>活动分享</li>
						    <li>活动积分</li>
						    <li>相关商品</li>
						  </ul>
						  <div class="layui-tab-content">
						    <div class="layui-tab-item layui-show">
						    	<div class="layui-form-item">
									<label class="layui-form-label">所属定位</label>
									<div class="layui-input-block">
										<select name="data[aid]" >
							            <?php foreach ($area as $v){?>
							            	<option value="<?php echo $v['id'];?>" <?php echo $v['id']==$item['aid']?'selected':''?>  ><?php echo $v['aname'];?></option>
							            <?php }?>
							        	</select>
									</div>
								</div>
								<div class="layui-form-item">
									<label class="layui-form-label">区域选择</label>
									<div class="layui-input-block">
										<select name="data[pt_aid]" >
							            <?php foreach ($ptarea as $v){?>
							            	<option value="<?php echo $v['id'];?>" <?php echo $v['id']==$item['pt_aid']?'selected':''?>><?php echo $v['aname'];?></option>
							            <?php }?>
							        	</select>
									</div>
								</div>
								<div class="layui-form-item">
									<label class="layui-form-label">活动分类</label>
									<div class="layui-input-block">
										<select name="data[tgid]" >
							            <?php foreach ($group as $v){?>
							            	<option value="<?php echo $v['id'];?>" <?php echo $v['id']==$item['tgid']?'selected':''?>><?php echo $v['tgname'];?></option>
							            <?php }?>
							        	</select>
									</div>
								</div>
								<div class="layui-form-item">
									<div class="layui-inline">
										<label class="layui-form-label">是否定位</label>
										<div class="layui-input-inline">
											<input type="hidden" name="data[position]" value="0">
											<input type="checkbox" name="data[position]" lay-skin="switch" lay-text="开启|关闭" <?php echo $item['position']==1?'checked':''?> value="1">
										</div>
									</div>
									<div class="layui-inline">
										<label class="layui-form-label">是否审核</label>
										<div class="layui-input-block">
											<input type="hidden" name="data[verify]" value="0">
											<input type="checkbox" name="data[verify]" lay-skin="switch" lay-text="开启|关闭"  <?php echo $item['verify']==1?'checked':''?>  value="1">
										</div>
									</div>
								</div>
								<div class="layui-form-item">
									<label class="layui-form-label">集合地点</label>
									<div class="layui-input-block">
									<?php foreach ($venue as $k=>$v){?>
										<input type="checkbox" name="data[tvid][<?php echo $k;?>]" class="tvname" title="<?php echo $v['tvname']?>" <?php echo in_array($v['id'], explode(',', $item['tvid']))?'checked':''?> value="<?php echo $v['id'];?>">
									<?php }?>
									</div>
								</div>
								<div class="layui-form-item">
									<label class="layui-form-label">主办单位</label>
									<div class="layui-input-block">
										<input type="text" name="data[company]" class="layui-input" value="<?php echo $item['company'];?>" >
									</div>
								</div>
								<div class="layui-form-item">
									<label class="layui-form-label">活动领队</label>
									<div class="layui-input-block">
										<select name="data[leader_uid]" >
							            <?php foreach ($user as $v){?>
							            	<option value="<?php echo $v['id'];?>" <?php echo $item['leader_uid']==$v['id']?'selected':'';?>><?php echo $v['nickname'];?></option>
							            <?php }?>
							        	</select>
									</div>
								</div>
								<div class="layui-form-item">
									<label class="layui-form-label">活动标题</label>
									<div class="layui-input-block">
										<input type="text" name="data[title]" class="layui-input" value="<?php echo $item['title'];?>" lay-verify="required" >
									</div>
								</div>
								<div class="layui-form-item">
									<label class="layui-form-label">拼团费用</label>
									<div class="layui-input-block">
										<input type="text" name="data[money]" class="layui-input" value="<?php echo $item['money'];?>" lay-verify="required" >
									</div>
								</div>
								<div class="layui-form-item">
									<div class="layui-inline">
										<label class="layui-form-label">活动天数</label>
										<div class="layui-input-inline">
											<input type="text" name="data[days]" class="layui-input" value="<?php echo $item['days'];?>" lay-verify="required|number">
										</div>
									</div>
									<div class="layui-inline">
										<label class="layui-form-label">难度</label>
										<div class="layui-input-inline">
											<input type="text" name="data[diff]" class="layui-input" value="<?php echo $item['diff'];?>" lay-verify="required|number">
										</div>
									</div>
								</div>
								<div class="layui-form-item">
									<div class="layui-inline">
										<label class="layui-form-label">开始时间</label>
										<div class="layui-input-inline">
											<input type="text" name="data[pt_stime]" class="layui-input time" value="<?php echo $item['pt_stime'];?>" lay-verify="required" >
										</div>
									</div>
									<div class="layui-inline">
										<label class="layui-form-label">截至时间</label>
										<div class="layui-input-inline">
											<input type="text" name="data[pt_etime]" class="layui-input time" value="<?php echo $item['pt_etime'];?>" lay-verify="required" >
										</div>
									</div>
								</div>
								<div class="layui-form-item">
									<label class="layui-form-label">活动图片</label>
									<div class="layui-input-block">
										<?php echo admin_btn(site_url('/images/upload'), 'file','layui-btn-primary f_file');?><span class="thumb-say">图片最大宽度640px</span>
										<div class="layui-upload layui-hide">
										  	<div class="layui-upload-list"><table class="layui-table"><tbody class="file_list"></tbody></table></div>
										  	<?php echo admin_btn('', 'file_sub','layui-btn-normal','');?>
									  	</div>
									  	<input name="data[thumb]" class="thumb" type="hidden" lay-verify='thumb' value="<?php echo $item['thumb'];?>" />
									</div>
								</div>
						    </div>
						    <div class="layui-tab-item">
						    	<div class="layui-form-item">
									<label class="layui-form-label">活动详情</label>
									<div class="layui-input-block">
										<script type="text/plain" id="emditor1" name="data[content]"><?php echo $item['content']; ?></script>
									</div>
								</div>
								<div class="layui-form-item">
									<label class="layui-form-label">活动行程</label>
									<div class="layui-input-block">
										<script type="text/plain" id="emditor2" name="data[journey]"><?php echo $item['journey']; ?></script>
									</div>
								</div>
								<div class="layui-form-item">
									<label class="layui-form-label">费用说明</label>
									<div class="layui-input-block">
										<script type="text/plain" id="emditor3" name="data[money_say]"><?php echo $item['money_say']; ?></script>
									</div>
								</div>
								<div class="layui-form-item">
									<label class="layui-form-label">免责说明</label>
									<div class="layui-input-block">
										<script type="text/plain" id="emditor4" name="data[disclaimer]"><?php echo $item['disclaimer']; ?></script>
									</div>
								</div>
						    </div>
						    <div class="layui-tab-item">
						    	<div class="layui-form-item">
									<label class="layui-form-label">分享标题</label>
									<div class="layui-input-block">
										<input type="text" name="data[s_title]" class="layui-input" placeholder="不填写默认显示活动标题" value="<?php echo $item['s_title']; ?>" >
									</div>
								</div>
								<div class="layui-form-item">
									<label class="layui-form-label">分享描述</label>
									<div class="layui-input-block">
										<input type="text" name="data[s_desc]" class="layui-input" value="<?php echo $item['s_desc']; ?>" placeholder="不填写默认显示活动标题" >
									</div>
								</div>
						    	<div class="layui-form-item">
									<label class="layui-form-label">分享图片</label>
									<div class="layui-input-block">
										<?php echo admin_btn(site_url('/images/upload'), 'file','layui-btn-primary f_file');?><span class="thumb-say">图片最大宽度640px</span>
										<div class="layui-upload layui-hide">
										  	<div class="layui-upload-list"><table class="layui-table"><tbody class="file_list"></tbody></table></div>
										  	<?php echo admin_btn('', 'file_sub','layui-btn-normal','');?>
									  	</div>
									  	<input name="data[s_thumb]" class="thumb" type="hidden" value="<?php echo $item['s_thumb']; ?>"/>
									</div>
								</div>
						    </div>
						    <div class="layui-tab-item">
						    	<blockquote class="layui-elem-quote">1 取消积分：是用户取消活动扣除的积分。2 分享次数：例如每天分享2次，即可获取2次相应积分。3 积分兑换：填写0.01 为1积分兑换1分RMB</blockquote>
						    	<div class="layui-form-item">
									<div class="layui-inline">
										<label class="layui-form-label">参与积分</label>
										<div class="layui-input-inline">
											<input type="text" name="data[integral_plus]" class="layui-input" value="<?php echo $item['integral_plus']; ?>" >
										</div>
									</div>
									<div class="layui-inline">
										<label class="layui-form-label">取消积分</label>
										<div class="layui-input-inline">
											<input type="text" name="data[integral_subtract]" class="layui-input" value="<?php echo $item['integral_subtract']; ?>" >
										</div>
									</div>
								</div>
						    	<div class="layui-form-item">
									<div class="layui-inline">
										<label class="layui-form-label">分享积分</label>
										<div class="layui-input-inline">
											<input type="text" name="data[share_plus]" class="layui-input" value="<?php echo $item['share_plus'];?>" >
										</div>
									</div>
									<div class="layui-inline">
										<label class="layui-form-label">分享次数</label>
										<div class="layui-input-inline">
											<input type="text" name="data[share_plus_day]" class="layui-input" value="<?php echo $item['share_plus_day'];?>" >
										</div>
									</div>
								</div>
						    	<div class="layui-form-item">
									<label class="layui-form-label">一积分兑换</label>
									<div class="layui-input-block">
										<input type="text" name="data[exchange]" class="layui-input" value="<?php echo $item['exchange'];?>" >
									</div>
								</div>
						    </div>
						    <div class="layui-tab-item">
						    	<div class="layui-form-item">
									<label class="layui-form-label">配件商品</label>
									<div class="layui-input-block">
									<?php foreach ($goods as $k=>$v){?>
										<input type="checkbox" name="data[gid][<?php echo $k;?>]" title="<?php echo $v['tgsname'].'(¥'.$v['tgsmoney'].')'?>" <?php echo in_array($v['id'], explode(',', $item['gid']))?'checked':''?> value="<?php echo $v['id']?>">
									<?php }?>
									</div>
								</div>
						    </div>
						  </div>
						</div>	
						<div class="layui-form-item">
							<div class="layui-input-block">
								<input type="hidden" name="id" value="<?php echo $item['id']?>"> 
								<?php echo admin_btn($edit_url,'save','layui-btn-lg',"lay-filter='sub' location='$index_url'")?>
							</div>
						</div>
			</form>
	</div>
</div>
<?php echo template('admin/script');?>
<script src="<?php echo PLUGIN.'umeditor/third-party/template.min.js'?>"></script>
<script src="<?php echo PLUGIN.'umeditor/umeditor.config.js'?>"></script>
<script src="<?php echo PLUGIN.'umeditor/umeditor.min.js'?>"></script>
<script src="<?php echo PLUGIN.'umeditor/lang/zh-cn/zh-cn.js'?>"></script>
<script type="text/javascript" src="<?php echo JS_PATH.'f_upload.js'?>" ></script>
<script type="text/javascript">

$('.f_file').f_upload();
function checkC(){
	if($('.tvname').is(":checked")){
		return true;
	}else{
		layer.msg("请选择一个集合点");
	}
	return false;
}

$(function(){
	$('.time').each(function(){
		layui.laydate.render({ 
			  elem: this,
			  type:'datetime',
			  format:"yyyy-MM-dd HH:mm"
		});	
	});
	var um1 = UM.getEditor('emditor1');
	um2 = UM.getEditor('emditor2');
	um3 = UM.getEditor('emditor3');
	um4 = UM.getEditor('emditor4');
	um1.setWidth("100%");um2.setWidth("100%");um3.setWidth("100%");um4.setWidth("100%");
	$(".edui-body-container").css("width", "98%");
});
</script>
<?php echo template('admin/footer');?>