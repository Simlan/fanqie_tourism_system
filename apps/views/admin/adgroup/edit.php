<?php echo template('admin/header');echo template('admin/sider');?>
<div class="layui-body">
	<div class="childrenBody childrenBody_show">
		<blockquote class="layui-elem-quote a-e-quote">
				<div class="layui-inline">编辑</div>
				<div class="layui-inline f-right"><?php echo admin_btn($index_url, '', 'layui-btn-xs','','返回')?></div>
		</blockquote>
		<form class="layui-form a-e-form" method="post">
			<div class="layui-form-item">
				<label class="layui-form-label"> 分类名称</label>
				<div class="layui-input-block">
					<input type="text" name="data[gname]" class="layui-input" value="<?php echo $item['gname']?>" lay-verify="required">
				</div>
			</div>

			<div class="layui-form-item">
				<label class="layui-form-label"> 分类别名</label>
				<div class="layui-input-block">
					<input type="text" name="data[say]" class="layui-input"  value="<?php echo $item['say']?>" >
				</div>
			</div>

			<div class="layui-form-item">
				<div class="layui-input-block">
				<input type="hidden" name="id" value="<?php echo $item['id']?>">
				<?php echo admin_btn($edit_url,'save','layui-btn-lg',"lay-filter='sub' location='$index_url'")?>
				</div>
			</div>
		</form>
	</div>
</div>
<?php echo template('admin/script');?>
<?php echo template('admin/footer');?>