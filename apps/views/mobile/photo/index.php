<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<?php echo template('mobile/header');?>
<div id="app">
	<van-nav-bar title="精彩相册"  left-arrow  @click-left="onClickLeft"  @click-right="onClickRight" class="mb10">
	  <van-icon name="wap-home" slot="right" />
	</van-nav-bar>	
	<template v-if="list">
	<van-list :finished="finished" class="mt10">
		<van-cell-group v-for="(v,index) in list" class="mt10">
			<van-cell :title="v.item.title" :url="'/mobile/photo/lists/id-'+v.item.id" is-link></van-cell>
			<van-cell :url="'/mobile/photo/lists/id-'+v.item.id" >
				<van-row gutter="10">
					<template v-if="v.thumb_arr">
					<van-col v-for="img in v.thumb_arr" span="8" >
						<lable class="lable-img" style="height:70px;"><img v-lazy="img" ></lable>
					</van-col>
					</template>
					<template v-else><p class="cr_hs2 text-center"><i class="iconfont icon-meiyoujieguo f20 d_block"></i><span v-text="'什么也木有~'"></span></p></template>
				</van-row>
				<div class="van-cell__label mt5 cr_hs2" v-text="'更新于 '+v.item.addtime"></div>
			</van-cell>
		</van-cell-group>
	</van-list>
	</template>
	<template v-else><p class="cr_hs2 text-center mt60"><i class="iconfont icon-meiyoujieguo f80 d_block"></i><span v-text="'什么也木有~'"></span></p></template>
	<div class="mt60">&nbsp;</div>
</div>
<?php echo template('mobile/script');?>
<?php echo template('mobile/share');?>
<script>
new Vue({
	el: '#app',
	data: {
		list: <?php echo $items?json_encode($items):'null';?>,
		finished:false
	},
  	methods: {
  	  	
  	},
  	mounted:function (){
	}
});
</script>
</body>
</html>