<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<?php echo template('mobile/header');?>
<div id="app">
	<van-nav-bar title="申请领队"  left-arrow  @click-left="onClickLeft"  @click-right="onClickRight" class="mb10">
	  <van-icon name="wap-home" slot="right" />
	</van-nav-bar>
	<van-row class="p10 bg_ff">
	  <van-col span="24">
	  	<?php echo $item['content'];?>
	  </van-col>
	</van-row>
	<template v-if="l_state==0">
		<van-cell-group >
	  		<van-field  v-model="username" clearable  label="真实姓名"  placeholder="请输入真实姓名"   ></van-field>
			<van-field  v-model="mobile"   clearable  label="手机号码"  placeholder="请输入手机号码"   ></van-field>
			<van-field  v-model="content"  clearable  label="个人简介"  placeholder="请输入个人简介" type="textarea" autosize rows="1"></van-field>
		</van-cell-group>
		<van-button type="primary" bottom-action class="van-contact-list-bottom" @click="l_state==0&&apply()" text="提交领队申请"></van-button> 
	</template>
	<template v-if="l_state==1">
	    <van-button type="primary" bottom-action class="van-contact-list-bottom"  text="正在审核中"></van-button> 
	</template> 
	<template v-else-if="l_state==2">
		<van-button type="primary" bottom-action class="van-contact-list-bottom" text="您的申请被拒绝"></van-button>  
	</template>
	<template v-else-if="l_state==3">
		<van-button type="primary" bottom-action class="van-contact-list-bottom" text="恭喜您已经是一名领队了"></van-button>  
	</template>
	
</div>
<div class="mb60">&nbsp;</div>
<?php echo template('mobile/script');?>
<script>
new Vue({
	el: '#app',
	data: {
		username:'',
		mobile:'',
		content:'',
		l_state:<?php echo $user['leader'];?>
	},
  	methods: {
  		apply(){
  			var items = ['username','mobile','content'];
    	    var isValid = items.every(item => {
    	        var msg = this.getErrorMessageByKey(item);
    	        if (msg) {
    	            this.$toast(msg);    	           
    	        }
    	        return !msg;
    	   	});
    	 	if(!isValid)return ;
    	 	var data = {"data[username]":this.username,"data[mobile]":this.mobile,"data[content]":this.content};
  			var l = this.$toast.loading({duration: 0,mask: true,message: '申请中...'});
  			axios.post('/mobile/user/leader',Qs.stringify(data),ajaxconfig).then((response)=> {
  	  	  	  	var data = response.data;
  	  	  	  	l.clear();
  	  	      	if(data.state==1){
  	  	  	    	this.l_state = 1;
  	  	  	  	}
  	  	  	    this.$toast(data.message);
  	  	    });
  	  	},
  	  	getErrorMessageByKey(key) {
	      switch (key) {
	        case 'username':
	          return this.username ? this.username.length <= 5 ? '' : '名字过长，请重新输入' : '请填写名字';
	        case 'mobile':
	          return this.telValidator(this.mobile) ? '' : '请填写正确的手机号码或电话号码';
	        case 'content':
		      return this.content ? '' : '请填写正确的身份证号码';
	      }
	    },
  		telValidator(value) {
	    	if (!value) return false;
        	value = value.replace(/[^-|\d]/g, '');
        	return /^((\+86)|(86))?(1)\d{10}$/.test(value) || /^0[0-9\-]{10,13}$/.test(value);
      	}	  	
  	},
  	mounted:function (){
  		
	}
});
</script>
</body>
</html>