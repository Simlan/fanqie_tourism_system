<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<?php echo template('mobile/header');?>
<div id="app">
	<van-row class="user_hbg bg_ff pt20 pb20 text-center cr_ff">
		<van-col span="24" ><img src="<?php echo $U['thumb'];?>" class="img_br3  wd20"></van-col>
		<van-col span="24" ><span class="nickname mauto pl10 pr10"><?php echo $U['nickname'];?> | 
		<?php if($gid==1){ echo '普通会员';}elseif($gid==2){ echo '领队';}else{ echo '领队兼分销会员';}?>
		</span>
		</van-col>
		<van-icon name="edit-data" class="setting" @click="edituser"></van-icon>
	</van-row>
    
    <van-row class="user-links text-center bg_ff pt10 pb10">
      <van-col span="6" >
        <a href="/mobile/tourism/my_order"><i class="iconfont icon-huodong f24 d_block"></i>全部活动</a>
      </van-col>
      <van-col span="6">
        <a href="/mobile/tourism/my_order/id-1"><i class="iconfont icon-woyibaoming f24 d_block"></i>已报名</a>
      </van-col>
      <van-col span="6">
        <a href="/mobile/tourism/my_order/id-2"><i class="iconfont icon-jieshu f24 d_block"></i>已结束</a>
      </van-col>
      <van-col span="6">
        <a href="/mobile/tourism/my_order/id-3"><i class="iconfont icon-tuichujiazuxianxing f24 d_block"></i>已退出</a>
      </van-col>
    </van-row>

    <van-cell-group class="mb10">
      <van-cell icon="records" title="商城订单" is-link @click="edit"></van-cell>
    </van-cell-group>

	<van-cell-group>
		<van-cell icon="gold-coin" title="开发者微信" is-link value="13538436848"></van-cell>
    	<van-cell icon="gold-coin" title="我的团队" is-link url="/mobile/user/fx_apply.html" ></van-cell>
      	<van-cell icon="gift" title="申请领队" is-link url="/mobile/user/leader.html" ></van-cell>
      	<van-cell icon="exchange" title="我的积分" is-link url="/mobile/user/integral.html" ></van-cell>
      	<van-cell icon="gift" title="旅游知识" is-link url="/mobile/news/lists/cid-3.html" ></van-cell>
      	<van-cell icon="exchange" title="商业合作" is-link url="/mobile/news/detail/id-25.html" ></van-cell>
      	<van-cell icon="cart" title="旅行商城" is-link @click="edit" ></van-cell>
      	<van-cell icon="gift" title="帮助说明" is-link url="/mobile/news/lists/cid-4.html" ></van-cell>
	</van-cell-group>
	
	<?php echo template('mobile/tabbar');?>
</div>

<?php echo template('mobile/script');?>
<script>
new Vue({
	el: '#app',
	data: {
		active:4
	},
  	methods: {
  		edituser(){
  	  		location.href = "/mobile/user/edit.html";
  	  	},
		edit(){
			this.$toast.fail("完善中...");
		}
  	},
  	mounted:function (){
  		
	}
});
</script>
</body>
</html>