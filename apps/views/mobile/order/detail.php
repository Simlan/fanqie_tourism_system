<?php $data['definedcss'] = array(CSS_PATH.'mobile/weui.min',CSS_PATH.'mobile/index',CSS_PATH.'mobile/mall'); echo template('mobile/header',$data);?>

<header>
	<div class="weui-cells" style="margin: -1px 0 15px 0;">
			<div class="weui-cell">
				<div class="weui-cell__bd">
					订单号：<?php echo $newItems['order_no'];?>
				</div>
				<div class="weui-cell__ft c-diy" ><?php if($newItems['state']==5){?>
							退货处理中
							<?php }elseif($newItems['state']==2){?>
							待发货
							<?php }elseif($newItems['state']==3){?>
							待收货
							<?php }elseif($newItems['state']==4){?>
							待评价
							<?php }?>
				</div>
			</div>
	</div>
	<div class="weui-cells order_affirm_address" style="padding: 0;">
			<div class="weui-cell">
				<div class="weui-cell__hd">
					<svg class="icon" aria-hidden="true"><use xlink:href="#icon-dizhi"></use></svg>
				</div>
				<div class="weui-cell__bd">
					<p class="names"><span class="cur_name"><?php echo $newItems['title']?></span>   &nbsp;&nbsp;&nbsp;  <span class="cur_phone"><?php echo $newItems['buy_mobile']?></span></p>
					<p class="address cur_address"><?php echo $newItems['buy_address'];?> </p>
				</div>
			</div>
	</div>
</header>
<div class="page order_affirm">
	<div class="weui-cells">
		<a class="weui-cell" href="<?php echo site_url('mobile/mall/detail/id-'.$newItems['id'])?>">
			<div class="weui-cell__hd">
				<img src="<?php echo $newItems['thumb'];?>" >
			</div>
			<div class="weui-cell__bd">
				<p class="names"><?php echo $newItems['title'];?></p>
			</div>
			<div class="weui-cell__ft">
				<p class="price">￥<?php echo $newItems['prices'];?></p>
				<p class='num'><?php echo 'X'.$newItems['num'];?></p>
			</div>
		</a>
		<div class="weui-cell" >
			<div class="weui-cell__hd">快递公司</div>
			<div class="weui-cell__bd"></div>
			<div class="weui-cell__ft"><?php echo $newItems['exp_company'];?></div>
		</div>
		<a class="weui-cell weui-cell_access" <?php if($newItems['exp_company']){ ?> href="https://m.kuaidi100.com/index_all.html?type=<?php echo $newItems['exp_company'];?>&postid=<?php echo $newItems['exp_no'];?>&callbackurl=<?php echo HTTP_HOST.$_SERVER['REQUEST_URI'];?>" <?php }?>>
			<div class="weui-cell__hd">快递单号</div>
			<div class="weui-cell__bd"></div>
			<div class="weui-cell__ft"><?php echo $newItems['exp_no']?$newItems['exp_no']:'安排发货中';?></div>
		</a>
		<div class="weui-cell" >
			<div class="weui-cell__hd">买家留言</div>
			<div class="weui-cell__bd">
				<?php echo $newItems['message'];?>
			</div>
		</div>
	</div>
	<div class="weui-cells">
		<div class="weui-cell" >
			<div class="weui-cell__bd">商品总额</div>
			<div class="weui-cell__ft c-diy" >￥<?php echo $newItems['price'];?></div>
		</div>
		<div class="weui-cell" >
			<div class="weui-cell__bd"></div>
			<div class="weui-cell__ft" style="font-size: 12px;">订单创建时间：<?php echo format_time($newItems['addtime']);?></div>
		</div>
	</div>
</div>
<?php echo template('mobile/script');?>
</body>
</html>