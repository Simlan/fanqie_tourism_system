<?php $data['definedcss'] = array(CSS_PATH.'mobile/weui.min',CSS_PATH.'mobile/index',CSS_PATH.'mobile/mall'); echo template('mobile/header',$data);?>

<div class="page order_lists">
	<div class="weui-tab">
            <div class="weui-navbar">
                <a class="weui-navbar__item <?php if(!$xz)echo 'weui-bar__item_on';?>" href="<?php echo site_url('mobile/order/lists')?>">全部</a>
                <a class="weui-navbar__item <?php if($xz==2)echo 'weui-bar__item_on';?>" href="<?php echo site_url('mobile/order/lists/id-2')?>">待发货</a>
                <a class="weui-navbar__item <?php if($xz==3)echo 'weui-bar__item_on';?>" href="<?php echo site_url('mobile/order/lists/id-3')?>">待收货</a>
                <a class="weui-navbar__item <?php if($xz==5)echo 'weui-bar__item_on';?>" href="<?php echo site_url('mobile/order/lists/id-5')?>">退款</a>
            </div>
            <div class="weui-tab__panel" >
            
            <?php if($newItems){ foreach ($newItems as $v){?>
				<div class="weui-cells center-list">
					<div class="weui-cell" >
						<div class="weui-cell__hd">
							订单号：<?php echo $v['order_no'];?>
						</div>
					</div> 
					<div class="weui-cell" >
						<div class="weui-cell__hd">
							<img src="<?php echo $v['thumb']?>">
						</div>
						<div class="weui-cell__bd"><p class="title"><?php echo str_cut($v['title'], 30)?></p></div>
						<div class="weui-cell__ft"><p class="prices"><?php echo '￥'.$v['prices']?></p><p class="num"><?php echo 'X'.$v['num']?></p></div>
					</div> 
					<div class="weui-cell" >
						<div class="weui-cell__bd"></div>
						<div class="weui-cell__ft" style="color: #fff;">
							<a href="<?php echo site_url('mobile/order/detail/id-'.$v['id'])?>" class="weui-btn weui-btn_mini weui-btn_primary">订单详情</a>
							<?php if($v['state']==1){?>
							<?php }elseif($v['state']==2){?>
							<a class="weui-btn weui-btn_mini weui-btn_warn">待发货</a>
							<?php }elseif($v['state']==3){?>
							<a href="<?php echo site_url('mobile/order/affirm_receive/id-'.$v['id'])?>" class="weui-btn weui-btn_mini weui-btn_warn ajaxproxy" method="get" tips="ok" location="reload" >确认收货</a>
							<a href="<?php echo site_url('mobile/order/back/id-'.$v['id'])?>" class="weui-btn weui-btn_mini weui-btn_warn ajaxproxy" method="get" tips="ok" location="reload">申请退货</a>
							<?php }elseif($v['state']==5){?>
							<a href="#" class="weui-btn weui-btn_mini weui-btn_warn">退货处理中</a>
							<?php }?>
						</div>
					</div>
				</div>
			<?php }}else{?>
				<div class="text-center" style="margin: 30% 10%">
					<p style="margin-bottom: 5%;color: #999;"></p>
				</div>
			<?php }?>
            </div>
	</div>
</div>
<div class="hr_70"></div>
<?php echo template('mobile/script');?>
<?php echo template('mobile/footer');?>
