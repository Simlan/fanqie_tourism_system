<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<?php $data['definedcss'] = array(CSS_PATH.'mobile/swiper-3.3.1.min',CSS_PATH.'mobile/sweetalert2.min',CSS_PATH.'mobile/index'); echo template('mobile/header',$data);?>
<div class="swiper-container swiper-container1">
	<div class="swiper-wrapper">
		<?php foreach ($aditems as $v){?>
		<div class="swiper-slide">
			<a href="<?php echo $v['url'];?>"> <img src="<?php echo $v['thumb'];?>"></a>
		</div>
		<?php }?>
	</div>
</div>

  <ul class="bg-ff plr12 mb60">
    <li>
      <a href="<?php echo site_url('mobile/pioneer/about')?>" class="flex-csb ptb15 bb-e5">
        <div class="flex-cs">
          <svg class="icon" aria-hidden="true" font-size="0.22rem">
            <use xlink:href="#icon-tuozhankehu-copy"></use>
          </svg>
          <span class="f15 flex-none pl8">关于开拓者</span>
        </div>
        <svg class="icon" aria-hidden="true" color="#666" font-size="0.18rem">
          <use xlink:href="#icon-more"></use>
        </svg>
      </a>
    </li>
    <li>
      <a href="<?php echo site_url('mobile/news/lists/cid-2')?>" class="flex-csb ptb15 bb-e5">
        <div class="flex-cs">
          <svg class="icon" aria-hidden="true" font-size="0.22rem">
            <use xlink:href="#icon-zixun"></use>
          </svg>
          <span class="f15 flex-none pl8">社会资讯</span>
        </div>
        <svg class="icon" aria-hidden="true" color="#666" font-size="0.18rem">
          <use xlink:href="#icon-more"></use>
        </svg>
      </a>
    </li>
    <li>
      <a href="<?php echo site_url('mobile/news/lists/cid-3')?>" class="flex-csb ptb15 bb-e5">
        <div class="flex-cs">
          <svg class="icon" aria-hidden="true" font-size="0.22rem">
            <use xlink:href="#icon-camping"></use>
          </svg>
          <span class="f15 flex-none pl8">旅行知识</span>
        </div>
        <svg class="icon" aria-hidden="true" color="#666" font-size="0.18rem">
          <use xlink:href="#icon-more"></use>
        </svg>
      </a>
    </li>
    <li>
      <a href="<?php echo site_url('mobile/user/leader')?>" class="flex-csb ptb15 bb-e5">
        <div class="flex-cs">
          <svg class="icon" aria-hidden="true" font-size="0.22rem">
            <use xlink:href="#icon-lingduiguanli"></use>
          </svg>
          <span class="f15 flex-none pl8">申请领队</span>
        </div>
        <svg class="icon" aria-hidden="true" color="#666" font-size="0.18rem">
          <use xlink:href="#icon-more"></use>
        </svg>
      </a>
    </li>
    <li>
      <a href="<?php echo site_url('mobile/mall/index')?>" class="flex-csb ptb15 bb-e5">
        <div class="flex-cs">
          <svg class="icon" aria-hidden="true" font-size="0.22rem">
            <use xlink:href="#icon-shangcheng-copy"></use>
          </svg>
          <span class="f15 flex-none pl8">旅行商城</span>
        </div>
        <svg class="icon" aria-hidden="true" color="#666" font-size="0.18rem"><use xlink:href="#icon-more"></use></svg>
      </a>
    </li>
    <li>
      <a href="<?php echo site_url('mobile/pioneer/team')?>" class="flex-csb ptb15 bb-e5">
        <div class="flex-cs">
          <svg class="icon" aria-hidden="true" font-size="0.22rem">
            <use xlink:href="#icon-fuwu"></use>
          </svg>
          <span class="f15 flex-none pl8">自由行服务</span>
        </div>
        <svg class="icon" aria-hidden="true" color="#666" font-size="0.18rem">
          <use xlink:href="#icon-more"></use>
        </svg>
      </a>
    </li>
  </ul>
<?php echo template('mobile/script');?>
<script type="text/javascript" src="<?php echo JS_PATH.'mobile/swiper-3.3.1.jquery.min.js'?>" ></script>
<script>
  var mySwiper = new Swiper ('.swiper-container1', {
    direction: 'horizontal',
    loop: false,
    autoplay:3000
  })
</script>
<?php echo template('mobile/footer');?>