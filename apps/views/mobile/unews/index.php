<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<?php echo template('mobile/header');?>
<div id="app">
	<van-swipe :autoplay="3000" class="bg_ff">
	  <van-swipe-item v-for="(v, index) in aditems" :key="index" class="text-center">
	  	<a :href="v.url"><img v-lazy="v.thumb" ><span class="swipe-title" v-text="v.title"></span></a>
	  </van-swipe-item>
	</van-swipe>
	<van-row class="pt10 pb5 bg_ff">
		<van-col  span="12" class="text-center mb5" >
			<a href="/mobile/unews/upload">
			<div>
				<i class="iconfont icon-fabu f40 cr-alreadygo"></i>
		  	</div>
			<div  v-text="'发布游记'"></div>
			</a>
		</van-col>
		<van-col  span="12" class="text-center mb5">
			<a href="/mobile/photo/index">
			<div>
				<i class="iconfont icon-xiangce f40 cr_main"></i>
		  	</div>
			<div  v-text="'精选相册'"></div>
			</a>
		</van-col>
	</van-row>
	<template v-if="list">
		<van-list   :finished="finished"   class="mt10">
			<van-cell-group class="unews">
				<van-cell v-for="(v,index) in list" :url="'/mobile/unews/detail/id-'+v.id">
					<van-col span="9"><lable class="lable-img" style="height:80px;"><img v-lazy="v.h_thumb" ></lable></van-col>
					<van-col span="15" class="pl10">
						<p v-text="v.title" class="title"></p>
						<p><img :src="v.thumb" class="wd15 brus50"> <span v-text="v.nickname" class="f12"></span></p>
					</van-col>
				</van-cell>
			</van-cell-group>
		</van-list>
	</template>
	<template v-else><p class="cr_hs2 text-center mt60"><i class="iconfont icon-meiyoujieguo f80 d_block"></i><span v-text="'什么也木有~'"></span></p></template>
	<?php echo template('mobile/tabbar');?>
</div>


<?php echo template('mobile/script');?>
<?php echo template('mobile/share');?>
<script>
new Vue({
	el: '#app',
	data: {
		active:2,
		aditems: '',
		list: <?php echo $items?json_encode($items):'null';?>,
	    loading: false,
	    finished: false
	},
  	methods: {
  		onLoad() {
    	      
    	},
  	    load_ad:function(){
			var that = this;
			//加载全部ajax		  	   		
			axios.all([get_ad()]).then(axios.spread(function (ad) {
				var aditem = ad.data;
				if(aditem.state==1){
		  			that.aditems = aditem.data;
			  	}else{
				  	that.ad_load = false;
			  		that.$toast(aditem.message);
				}
			}));
	    }
  	},
  	mounted:function (){
  		this.load_ad();
	}
});

function get_ad() {//获取广告
	  return axios.post('/api/home/ad',Qs.stringify({cid:6}),ajaxconfig);
}
</script>
</body>
</html>