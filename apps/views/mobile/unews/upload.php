<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<?php echo template('mobile/header');?>
<style>
video{width: 100%;height:auto;background-color: #000;}
audio{width: 100%;}
img{width: 100%}
video::-webkit-media-controls-enclosure {overflow:hidden;}
video::-webkit-media-controls-panel {width: calc(100% + 40px);}
audio::-webkit-media-controls-enclosure {overflow:hidden;}
audio::-webkit-media-controls-panel {width: calc(100% + 40px);}
</style>
<div id="app">
	<van-nav-bar title="发布游记"  left-arrow  @click-left="onClickLeft"  @click-right="onClickRight" class="mb10">
	  <van-icon name="wap-home" slot="right" />
	</van-nav-bar>
	
	<van-row class="bg_ff">
		<van-col  span="24" class="text-center">
			<van-uploader :after-read="onHead" @oversize="onSize('8M')" max-size="8388608" style="padding:100px 0;" v-show="upload_btn" accept="image/*" >
			  <van-icon name="photograph" class="f16 icon-bottom"></van-icon><span class="f14 icon-bottom" v-text="' 添加封面图'"></span>
			</van-uploader>
			<div v-show="upload_img">
				<lable class="lable-img" style="height:216px;"><img :src="img_view" ></lable>
			</div>
		</van-col>
	</van-row>
	
	<van-row class="mt10 unews-u-title">
		<van-col  span="24" >
			<van-cell-group>
			  <van-field  v-model="title" left-icon="is iconfont icon-icon-- f16 van-field__icon"  placeholder="为您的游记取个名字吧..."  ></van-field>
			</van-cell-group>
		</van-col>
	</van-row>
	
	<template v-if="music">
	  		<div class="mt10" v-html="music"></div>
	</template>
	<template v-else>
	  	<van-uploader :after-read="addMusic" @oversize="onSize('8M')" max-size="8388608"  accept="audio/*" class="wd100">
		  	<van-cell-group class="mt10">
			  	<van-cell title="插入音乐" is-link >
				    <template slot="icon">
				      <i class="iconfont icon-yinlemusic214 mr15" ></i>
				    </template>
			  	</van-cell>
		  	</van-cell-group>
	  	</van-uploader>
	</template>
	
	<template v-if="content.length">
		<div v-for="(v,index) in content" class="unews-u-con"  @click="onEdit(index)">
			<div class="bg_ff m5 p10" v-html="v"></div>
			<div class="close" @click="onClose(index)"><van-icon name="clear" ></van-icon></div>
		</div>
	</template>
	
	<van-row class="mt15 mb60 text-center unews-u-add ml10 mr10" >
		<van-col  span="8" >
			<div class="bg_ff mr10 pt10 pb10" >
				<van-uploader :after-read="addPhoto"  accept="image/*" @oversize="onSize('8M')" max-size="8388608" multiple >
					<p><i class="iconfont f30 icon-xiangji"></i></p>
					<p>添加照片</p>
				</van-uploader>
			</div>
		</van-col>
		<van-col  span="8" >
			<div class="bg_ff mr5 ml5 pt10 pb10" @click="onAddText">
				<p><i class="iconfont f30 icon-wenzi"></i></p><p>添加文字</p>
			</div>
		</van-col>
		<van-col  span="8" >
			<div class="bg_ff ml10 pt10 pb10">
				<van-uploader :after-read="addVideo" @oversize="onSize('8M')" max-size="8388608"  accept="video/*" >
					<p><i class="iconfont f30 icon-shipin"></i></p>
					<p>添加视频</p>
				</van-uploader>
			</div>
		</van-col>
	</van-row>
	<van-popup v-model="show" position="top" class="unews-d-sub">
		<van-col span="24">
	  		<van-field  v-model="text_con" type="textarea" placeholder="添加文本" rows="12"></van-field>
	  	</van-col>
	  	<van-col span="24" class="mb15 text-center">
	  		<van-button type="default" @click="show=false">关闭</van-button> <van-button type="primary" class="ml15" @click="addText">添加</van-button>
	  	</van-col>
	</van-popup>
	<div class="mt60">&nbsp;</div>
	<van-row class="van-contact-list-bottom ">
	 	  <van-col span="24">
		    <van-button bottom-action :loading="sub_load" type="danger" @click="submit">提交发布</van-button>
		  </van-col>
	</van-row>
	
	<van-popup v-model="show_load" style="background-color:rgba(255, 255, 255, 0);" :close-on-click-overlay="false" :z-Index = "10000">
		<van-circle  v-model="progresss"  :rate="progress" :speed="100" color="#13ce66" :text="load_text" fill="#fff"  size="120px"  layer-color="#eee"></van-circle>
	</van-popup>
	
</div>

<?php echo template('mobile/script');?>
<script type="text/javascript" src="<?php echo JS_PATH.'mobile/f_yasuo.js'?>" ></script>
<script>
var config = {
	onUploadProgress: progressEvent => {
		   var complete = (progressEvent.loaded / progressEvent.total * 100 | 0);
		   r.progress = complete;
		   r.load_text = r.progress+'%';
	}
}
var r = new Vue({
	el: '#app',
	data: {
		upload_btn:true,upload_img:false,img_view:'',title:'',
		content:[],show:false,text_con:'',text_edit:'',music:'',progress:0,progresss:0,show_load:false,load_text:'',sub_load:false
	},
  	methods: {
  	  	submit(){
  	  	  	if(!this.img_view||!this.title||!this.content){
  	  	  	  	this.$toast("请检查封面，标题，内容是否都有添加");
  	  	  	  	return false;
  	  	  	}
  	  	  	this.sub_load = true;
  	  	  	this.content.unshift(this.music);
  	  	  	var data = {"data[h_thumb]":this.img_view,"data[title]":this.title,"content":this.content};
  	  	  	axios.post('/mobile/unews/upload',Qs.stringify(data),ajaxconfig).then((response)=> {
  	  	  	  	var data = response.data;
  	  	      	if(data.state==1){
  	  	  	      	this.$toast(data.message);
  		  	      	location.href = "/mobile/unews/index";
  	  	  	  	}else{
  					this.$toast(data.message);
  	  	  	  	}
  	  	     });
  	  	},
  		onClose(index){
  			this.content.splice(index,1)
  	  	},
  		onEdit(index){
  	  		if(event.target.title == 'text'){
  	  	  		this.show = true;
  	  	  	  	this.text_con = htmltotext(event.target.innerHTML);
  	  	  	  	this.text_edit = index;
  	  	  	}
  	  	},
  	  	onAddText(){
			this.show = true;
			this.text_con = '';
			this.text_edit = '';
       	},
    	addText(index){
        	if(this.text_con){
            	if(this.text_edit === ''){
            		this.content.push('<p  title="text">'+texttohtml(this.text_con)+'</p>');
                }else{
                	this.content[this.text_edit] = '<p  title="text">'+texttohtml(this.text_con)+'</p>';
                	this.text_edit = '';
                }
            }
        	this.text_con = '';
        	this.show = false;
        },
        onSize(size){
        	this.$toast("文件过大，不能超过"+size);
        },
        onHead(file) {
        	this.show_load = true;
        	var img = new Image();
            img.src = window.URL.createObjectURL(file.file);
            img.onload = ()=>{
                 compressSrc = compress(img,0.5,100,file.file);
	        	 var fd = new FormData();
	        	 fd.append('file', compressSrc,compressSrc.name);
	        	 axios.post('/api/unews/upload',fd,config).then((response)=> {
		  	  	  	var data = response.data;
		  	      	if(data.state==1){
			  	      	this.upload_btn = false;
			  		    this.upload_img = true;
			  		    this.img_view = data.data.url;
		  	  	  	}else{
						this.$toast(data.message);
		  	  	  	}
			  	    this.progress = 0;this.show_load = false;
		  	     });
            }
    	},
        addVideo(file){
    		this.show_load = true;
        	var fd = new FormData()
        	fd.append('file', file.file);
        	axios.post('/api/unews/upload',fd,config).then((response)=> {
	  	  	  	var data = response.data;
	  	      	if(data.state==1){
			  	    this.content.push('<p><video src="'+data.data.url+'" controls="controls" x5-video-player-type="h5" ></video></p>');
	  	  	  	}else{
					this.$toast(data.message);
	  	  	  	}
		  	    this.progress = 0;this.show_load = false;
	  	    });	
        },
        addPhoto(file){
            if(file.length > 3){
                this.$toast("一次最多上传3张图片");
            }else{
                this.show_load = true;
                var arr = [],html = '';
                if(!file.length){arr.push(file)}else{arr = file;}
                var requests = arr.map(uploads);
            }
        },
        addMusic(file){
        	this.show_load = true;
        	var fd = new FormData()
        	fd.append('file', file.file);
        	axios.post('/api/unews/upload',fd,config).then((response)=> {
	  	  	  	var data = response.data;
	  	      	if(data.state==1){
		  	      	this.music = '<p><audio src="'+data.data.url+'" controls="controls"></audio></p>'; 
	  	  	  	}else{
					this.$toast(data.message);
	  	  	  	}
		  	    this.progress = 0;this.show_load = false;
	  	    });	
        }
  	}
});

function uploads(file) {
	var img = new Image(); 
	var fd = new FormData();
    img.src = window.URL.createObjectURL(file.file);     
    img.onload = ()=>{
    	compressSrc = compress(img,0.5,100,file.file);
      	fd.append('file', compressSrc,compressSrc.name);
      	axios.post('/api/unews/upload',fd,config).then((response)=> {
  	  	  	var data = response.data;
  	      	if(data.state==1){
  	  	  	    r.content.push('<p><img src="'+data.data.url+'"></p>');
  	  	  	}else{
				r.$toast(data.message);
  	  	  	}
    	    r.progress = 0;r.show_load = false;
  	     });
    }
}

function texttohtml(str) {
    str = str.replace(/\r\n/g,"<br>");
    str = str.replace(/\n/g,"<br>");
    str = str.replace(/\s/g,"&nbsp;");
	return str;
}

function htmltotext(str) {
	str = str.replace(/<br>/g,"\r\n");
	str = str.replace(/<br>/g,"\n");
	str = str.replace(/&nbsp;/g," ");
	return str;
}
</script>
</body>
</html>