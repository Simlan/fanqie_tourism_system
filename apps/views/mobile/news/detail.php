<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<?php echo template('mobile/header');?>
<div id="app">
	<van-nav-bar title="<?php echo $item['title'];?>"  left-arrow  @click-left="onClickLeft"  @click-right="onClickRight" class="mb10">
	  <van-icon name="wap-home" slot="right" />
	</van-nav-bar>
	
	<van-row class="p10 bg_ff">
	  <van-col span="24">
	  	<?php echo $item['content'];?>
	  </van-col>
	</van-row>
	
</div>
<?php echo template('mobile/script');?>
<script>
new Vue({
	el: '#app'
});
</script>
</body>
</html>