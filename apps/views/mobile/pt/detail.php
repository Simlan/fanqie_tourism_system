<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<?php echo template('mobile/header'); ?>
<div id="app">
	<van-row class="bg_ff td_header cr_ff">
		<van-col span="24" class="text-center"><a  :href="photo?'/mobile/photo/lists/id-'+item.id:'#'"><img v-lazy="item.thumb" ></a></van-col>
		<van-col v-if="photo" span="24" class="img_num text-right f14 p10"><a  :href="'/mobile/photo/lists/id-'+item.id"><span><van-icon name="photo" class="mr5" ></van-icon><cc v-text="photo+'张'"></cc></span></a></van-col>
		<van-col span="24" class="title f16 p10 ellipsis"><p v-text="item.title" ></p></van-col>
	</van-row>
	
	<van-row class="bg_ff mb10 p10 f14">
		<van-col span="24" class="mt10">
			<div class=""><span v-text="'费用：'"></span><span class="cr_red" v-text="item.money"></span><span v-text="'元/人起'"></span></div>
		</van-col>
		<van-col span="24" class="mt5" >
			<span v-text="'难度：'"></span><i class="iconfont icon-xingxing cr_main" v-for="d in parseInt(item.diff)" ></i> <span class="cr_main" v-text="'（'+item.diff_say+'）'"></span> <span class="cr-alreadygo" v-text="item.integral_plus_say"></span>
		</van-col>
		<van-col span="24" class="mt5">
			<div class="ct-flexbox"><div class="ct-flexbox-item" v-text="'集合：'+item.venue"></div></div>
		</van-col>
		<van-col span="24" class="mt5">
			<div class="ct-flexbox"><div class="ct-flexbox-item" v-text="'说明：发起后邀请'+item.line_num+'人报名活动上线，'+item.in_line+'人成行'"></div></div>
		</van-col>
	</van-row>
	
	<van-tabs active="0" sticky class="bg_ff mb60">
		<van-tab title="活动详情" class="td_content">
			<p v-html="item.content" ></p>
		</van-tab>
		<van-tab title="行程准备" class="td_content">
			<p v-html="item.journey"  ></p>
		</van-tab>
		<van-tab title="费用说明" class="td_content">
			<p v-html="item.money_say" ></p>
		</van-tab>
		<van-tab title="评价">
			无
		</van-tab>
	</van-tabs>
	
	<van-goods-action>
		<van-goods-action-mini-btn icon="home" text="返回" @click="onClickLeft" ></van-goods-action-mini-btn>
	  	<van-goods-action-big-btn text="下一步，选时间"  primary :url="'/mobile/pt/time/id-'+item.id" ></van-goods-action-big-btn>
	</van-goods-action>
</div>
<?php echo template('mobile/script');?>
<?php echo template('mobile/share');?>
<script src="<?php echo JS_PATH.'jquery.min.js'?>"></script>
<script src="<?php echo JS_PATH.'mobile/lazyload.min.js'?>"></script>
<script>
var l = vant.Toast.loading({duration: 0,mask: true,message: '加载中...'});
new Vue({
	el: '#app',
	data: {
		item:<?php echo json_encode($item);?>,
		photo:0,
		id:<?php echo $id;?>
	},
  	methods: {
 		
 	
  	},
  	mounted:function (){
  		axios.all([tourism_photo(this.id)]).then(axios.spread( (photo)=> {
			this.photo = photo.data.data;
			l.clear();
		}));
	}
});
function tourism_photo(id) {
	  return axios.post('/api/home/tourism_photo',Qs.stringify({id:id}),ajaxconfig);
}
$("img").each(function () {
	var oImgSrc = $(this).attr("src");
	$(this).attr("src", "").attr("data-original", function () {
		return oImgSrc;
	}); 
});
$("img").lazyload({effect: "fadeIn"});
</script>
</body>
</html>