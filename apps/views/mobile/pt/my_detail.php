<?php use GuzzleHttp\json_decode;
$data['definedcss'] = array(CSS_PATH.'mobile/weui.min',CSS_PATH.'mobile/index',CSS_PATH.'mobile/mall'); echo template('mobile/header',$data);?>

<header>
	<div class="weui-cells" style="margin: -1px 0 15px 0;">
			<div class="weui-cell">
				<div class="weui-cell__bd">
					订单号：<?php echo $newItems['oid'];?>
				</div>
				<div class="weui-cell__ft" style="color: #ff547b;"><?php if($newItems['state']==5){?>
							退款处理中
							<?php }elseif($newItems['state']==2){?>
							已报名
							<?php }elseif($newItems['state']==3){?>
							已结束
							<?php }elseif($newItems['state']==4){?>
							已退出
							<?php }?>
				</div>
			</div>
	</div>
	
	<div class="weui-cells order_affirm_address" style="padding: 0;background:#fff none;">
	<?php $info = json_decode($newItems['info'],true); foreach ($info as $v){?>
			<div class="weui-cell">
				<div class="weui-cell__hd">
					<svg class="icon" aria-hidden="true"><use xlink:href="#icon-renyuan"></use></svg>
				</div>
				<div class="weui-cell__bd">
					<p class="names"><span class="cur_name"><?php echo $v['username']?></span>   &nbsp;&nbsp;&nbsp;  <span class="cur_phone"><?php echo $v['mobile']?></span></p>
					<p class="address cur_address"><?php echo $v['code'];?> </p>
				</div>
			</div>
			<?php }?>
	</div>
</header>
<div class="page order_affirm">
	<div class="weui-cells">
		<a class="weui-cell" href="<?php echo site_url('mobile/pt/detail/id-'.$newItems['tid'])?>">
			<div class="weui-cell__hd">
				<p class="names"><?php echo $newItems['title'];?></p>
			</div>
			<div class="weui-cell__bd"></div>
			<div class="weui-cell__ft">
				<p class="">￥<?php echo $newItems['money'];?></p>
			</div>
		</a>
		<div class="weui-cell" >
			<div class="weui-cell__hd">活动时间</div>
			<div class="weui-cell__bd" style="color: #999;margin-left: 10px;"><p><?php echo $newItems['stime'];?></p></div>
			<div class="weui-cell__ft"></div>
		</div>
		<a class="weui-cell"  >
			<div class="weui-cell__hd">集合方式</div>
			<div class="weui-cell__bd" >
			</div>
			<div class="weui-cell__ft" ><p onclick="showModal('.modal-pane');">点击查看</p></div>
		</a>
	</div>
	<?php $goods = json_decode($newItems['tgs_goods'],true);if($goods){?>?>
	<div class="weui-cells">
		<?php foreach ($goods as $v){?>
		<div class="weui-cell" >
			<div class="weui-cell__hd"><?php echo $v['tgsname'];?></div>
			<div class="weui-cell__bd" style="text-align: center;"><?php echo $v['tgsmoney'];?></div>
			<div class="weui-cell__ft"><?php echo 'x'.$v['num'];?></div>
		</div>
		<?php }?>
	</div>
	<?php }?>
	<div class="weui-cells">
		<div class="weui-cell" >
			<div class="weui-cell__bd">商品总额</div>
			<div class="weui-cell__ft" style="color: #ff547b;font-weight: bold;">￥<?php echo $newItems['total'];?></div>
		</div>
		<div class="weui-cell" >
			<div class="weui-cell__bd"></div>
			<div class="weui-cell__ft" style="font-size: 12px;">订单创建时间：<?php echo format_time($newItems['addtime']);?></div>
		</div>
	</div>
</div>

<div class="mask flex-center" style="display:none">
  <div class="modal-pane gather-pane w80 plr12 ptb12 br8 bg-ff flex-dc" style="display:none;font-size: 14px;">
    <button class="maskHide-btn fw-blod" onclick="hideModal('.modal-pane');">&times;</button>
    <ul>
    <?php $tv = json_decode($newItems['tv'],true);?>
      <li class="gather-list b-e5 br5 mtb12">
        <ul>
          <li class="plr12 ptb6 flex jc-sb ai-fs">
            <span class="gather-title c9 flex-none ta-right">集合点：</span>
            <span class="fg1"><?php echo $tv['tvname'];?></span>
          </li>
          <li class="plr12 ptb6 flex jc-sb ai-fs">
            <span class="gather-title c9 flex-none ta-right">导游：</span>
            <span class="fg1"><?php echo $tv['tvusername'];?></span>
          </li>
          <li class="plr12 ptb6 flex jc-sb ai-fs">
            <span class="gather-title c9 flex-none ta-right">联系方式：</span>
            <span class="fg1"><?php echo $tv['tvmobile'];?></span>
          </li>
          <li class="plr12 ptb6 flex jc-sb ai-fs">
            <span class="gather-title c9 flex-none ta-right">集合时间：</span>
            <span class="fg1"><?php echo $tv['tvtime'];?></span>
          </li>
          <li class="plr12 ptb6 flex jc-sb ai-fs">
            <span class="gather-title c9 flex-none ta-right">具体地址：</span>
            <span class="fg1"><?php echo $tv['tvaddress'];?></span>
          </li>
        </ul>
      </li>
    </ul>
  </div>
  </div>
<?php echo template('mobile/script');?>
<?php echo template('mobile/footer');?>