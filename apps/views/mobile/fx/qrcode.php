<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<?php echo template('mobile/header');?>
<div id="app">
	<van-nav-bar title="我的二维码"  left-arrow  @click-left="onClickLeft"  @click-right="onClickRight" class="mb10">
	  <van-icon name="wap-home" slot="right" />
	</van-nav-bar>
	<van-row class="text-center mb10">
		<van-col span="24" >
			<img src="<?php echo $img;?>" style="width: 100%;">
		</van-col>
	</van-row>
	<van-panel title="二维码说明：" >
	  <div class="p10 cr_888">	  
	  	<p>1、该二维码属于您的独享二维码</p>
	  	<p>2、长按二维码保存到本地后，可以发送给朋友、朋友圈、微信群、互联网</p>
	  	<p>3、任何人通过本二维码进入小程序购买商品，您都会获取相应的返佣</p>
	  	<p>4、获取返佣的方式并非只有通过二维码，也可以通过分享商品获取</p>
	  	<p>5、获取佣金的前提必须是分销会员</p>
	  </div>
	</van-panel>
</div>

<?php echo template('mobile/script');?>
<script>
new Vue({
	el: '#app'
});
</script>
</body>
</html>