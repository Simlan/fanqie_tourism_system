<?php
if (! defined ( 'BASEPATH' ))exit ( 'No direct script access allowed' );
/**
 * 游记评论管理
 * @author chaituan@126.com
 */
class TravelCmt_model extends MY_Model {
	function __construct() {
		parent::__construct ();
		$this->table_name = 'travel_cmt';
	}

}