<?php
if (! defined ( 'BASEPATH' ))exit ( 'No direct script access allowed' );
/**
 * 地区管理
 * @author chaituan@126.com
 */
class Area_model extends MY_Model {
	function __construct() {
		parent::__construct ();
		$this->table_name = 'area';
	}
	
	function cache() {
		$items = $this->getItems();
		set_Cache('area',$items);
	}
}