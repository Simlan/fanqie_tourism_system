<?php
if (! defined ( 'BASEPATH' ))exit ( 'No direct script access allowed' );
/**
 * 旅游
 * @author chaituan@126.com
 */
class Tourism_model extends MY_Model {
	function __construct() {
		parent::__construct ();
		$this->table_name = 'tourism';
	}
}