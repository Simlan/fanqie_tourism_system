<?php
if (! defined ( 'BASEPATH' ))exit ( 'No direct script access allowed' );
/**
 * 积分
 * @author chaituan@126.com
 */
class Integral_model extends MY_Model {
	function __construct() {
		parent::__construct ();
		$this->table_name = 'integral';
	}
}