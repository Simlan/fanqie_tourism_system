<?php
if (! defined ( 'BASEPATH' ))exit ( 'No direct script access allowed' );
/**
 * 拼团
 * @author chaituan@126.com
 */
class Pt_model extends MY_Model {
	function __construct() {
		parent::__construct ();
		$this->table_name = 'pt';
	}
}