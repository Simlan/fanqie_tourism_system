<?php
if (! defined ( 'BASEPATH' ))exit ( 'No direct script access allowed' );
/**
 * 协议
 * @author chaituan@126.com
 */
class Protocol_model extends MY_Model {
	function __construct() {
		parent::__construct ();
		$this->table_name = 'protocol';
	}
}