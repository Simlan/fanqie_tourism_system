<?php
if (! defined ( 'BASEPATH' ))exit ( 'No direct script access allowed' );
/**
 * 商业
 * @author chaituan@126.com
 */
class Business_model extends MY_Model {
	function __construct() {
		parent::__construct ();
		$this->table_name = 'business';
	}
}