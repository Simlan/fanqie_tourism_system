<?php
if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );
/**
 * 活动申请
 * @author chaituan@126.com
 *        
 */
class TourismApply_model extends MY_Model {
	public function __construct() {
		parent::__construct ();
		$this->table_name = 'tourism_apply';
	}
}