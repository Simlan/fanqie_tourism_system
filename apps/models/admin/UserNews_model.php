<?php
if (! defined ( 'BASEPATH' ))exit ( 'No direct script access allowed' );
/**
 * 用户文章管理
 * @author chaituan@126.com
 */
class UserNews_model extends MY_Model {
	function __construct() {
		parent::__construct ();
		$this->table_name = 'user_news';
	}
}