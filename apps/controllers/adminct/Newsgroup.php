<?php
defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );
/**
 * 文章分组
 * @author chaituan@126.com
 */
class Newsgroup extends AdminCommon {
	public function __construct() {
		parent::__construct ();
		$this->load->model ('admin/NewsCat_model','do');
	}
	
	public function index() {
		$items = $this->do->get_cat(false);
		$data ['items'] = $items;
		$this->load->view ('admin/newsgroup/index', $data);
	}
	
	public function add() {
		if (is_ajax_request ()) {
			$data = Posts('data');
			$data ['addtime'] = time();
			if ($this->do->add($data)) {
				$this->do->cache();
				AjaxResult_ok();
			} else {
				AjaxResult_error();
			}
		} else {
			$items = $this->do->get_cat();
			$data['parent'] = $items;
			$this->load->view ('admin/newsgroup/add', $data);
		}
	}
	
	public function edit() {
		if (is_ajax_request()) {
			$data = Posts('data');
			if ($this->do->updates($data,"id=".Posts('id','checkid'))) {
				$this->do->cache();
				AjaxResult_ok();
			} else {
				AjaxResult_error();
			}
		} else {
			$data['item'] = $this->do->getItem(array('id' => Gets('id','checkid')));
			$items = $this->do->get_cat();
			$data['parent'] = $items;
			$this->load->view('admin/newsgroup/edit',$data);
		}
	}
	
	public function del() {
		if($this->do->deletes("id=".Gets('id','checkid'))){
			$this->do->cache();
			AjaxResult_ok();
		}else{
			AjaxResult_error();
		}
	}
}
