<?php
defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );
/**
 * 集合点
 * @author chaituan@126.com
 */
class Photogroup extends AdminCommon {
	
	public function __construct() {
		parent::__construct();
		$this->load->model('admin/PhotoGroup_model','do');
	}
	
	public function index() {
		$this->load->view('admin/photogroup/index');
	}
	
	function lists(){
		$name = Gets('name');
		$page = Gets('page','checkid');$limit = Gets('limit','checkid');
		$total = Gets('total','num');
		$where = $name?"pname like '%$name%'":'';
		$data = $this->do->getItems($where,'','id desc',$page,$limit,$total);
		$find = Gets('find');
		if(($name&&$find)||!$total){
			$total = $this->do->count;
		}
		f_ajax_lists($total, $data);
	}
	
	public function add() {
		if (is_ajax_request ()) {
			$data = Posts('data');
			if ($this->do->add($data)) {
				AjaxResult_ok();
			} else {
				AjaxResult_error();
			}
		} else {
			$this->load->view ('admin/photogroup/add');
		}
	}
	
	public function edit() {
		if (is_ajax_request()) {
			$data = Posts('data');
			if ($this->do->updates($data,"id=".Posts('id','checkid'))) {
				AjaxResult_ok();
			} else {
				AjaxResult_error();
			}
		} else {
			$data['item'] = $this->do->getItem(array('id'=>Gets('id','num')));
			$this->load->view('admin/photogroup/edit',$data);
		}
	}
	
	public function del() {
		$id = Gets('id','checkid');
		$result = $this->do->deletes("id=$id");
		is_AjaxResult($result);
	}
}
