<?php
defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );
/**
 * 广告分组
 * @author chaituan@126.com
 */
class Adgroup extends AdminCommon {
	public function __construct() {
		parent::__construct ();
		$this->load->model ('admin/adGroup_model','do');
	}
	
	public function index() {
		$data ['items'] = $this->do->getItems();
		$this->load->view ('admin/adgroup/index', $data);
	}
	
	public function add() {
		if (is_ajax_request ()) {
			$data = Posts('data');
			$data ['addtime'] = time();
			if ($this->do->add($data)) {
				$this->do->cache();
				AjaxResult_ok();
			} else {
				AjaxResult_error();
			}
		} else {
			$this->load->view ('admin/adgroup/add');
		}
	}
	
	public function edit() {
		if (is_ajax_request()) {
			$data = Posts('data');
			if ($this->do->updates($data,"id=".Posts('id','checkid'))) {
				$this->do->cache();
				AjaxResult_ok();
			} else {
				AjaxResult_error();
			}
		} else {
			$data['item'] = $this->do->getItem(array('id'=>Gets('id','num')));
			$this->load->view('admin/adgroup/edit',$data);
		}
	}
	
	public function del() {
		if($this->do->deletes("id=".Gets('id','checkid'))){
			$this->do->cache();
			AjaxResult_ok();
		}else{
			AjaxResult_error();
		}
	}
}
