<?php
defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );
/**
 * @author chaituan@126.com
 */
class Contact extends CI_Controller {
	
	function __construct(){
		parent::__construct();
		$this->load->model(array("admin/Info_model"=>"do"));
	}
	
	function lists(){
		if(is_ajax_request()){
			$uid = Posts('uid');
			$info = $this->do->getItems(array('uid'=>$uid),'','id desc');
			if($info){
				AjaxResult(1,'ok', $info);
			}else{
				AjaxResult(2, '没有数据，请添加');
			}
			
		}
	}
	
	function add() {
		if(is_ajax_request()){
			sleep(2);
			$data = Posts('d');
			$result = $this->do->add($data);
			is_AjaxResult($result,'ok','添加数据失败');
		}
	}
	
	function edit(){
		if(is_ajax_request()){
			sleep(2);
			$data = Posts('d');
			$id = Posts('id','num');
			$result = $this->do->updates($data,array('id'=>$id));
			is_AjaxResult($result,'ok','修改数据失败');
		}
	}
	
	function del(){
		sleep(2);
		$id = Posts('id','num');
		$result = $this->do->deletes(array('id'=>$id));
		is_AjaxResult($result,'ok','删除数据失败');
	}
	
	
	
}
