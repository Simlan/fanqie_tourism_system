<?php
defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );
/**
 * @author chaituan@126.com
 */
class Home extends CI_Controller {
	
	function ad() {
		if(is_ajax_request()){
			$this->load->model('admin/Ad_model','do');
			$cid = Posts('cid','num');
			$items = $this->do->getItems(array('catid'=>$cid,'state'=>1));
			if($items){
				AjaxResult(1,'ok',$items);
			}else{
				AjaxResult_error('广告位没有数据');
			}
		}
	}
	
	function imenu(){
		if(is_ajax_request()){
			$this->load->model ('admin/TourismGroup_model','do');
			$items = $this->do->getItems(array('state'=>1),'','sort');
			if($items){
				AjaxResult(1,'ok',$items);
			}else{
				AjaxResult_error('菜单没有数据');
			}
		}
	}
	//首页活动数据
	function tourism(){
		if(is_ajax_request()){
			$lid = Posts('lid');
			$time = format_time(time(),'Y-m-d');
			$this->load->model(array('admin/Tourism_model'=>'do'));
			$table = array('ct_tourism_child as b'=>"tourism.id=b.tid+left");
			$fields = 'tourism.id,tourism.thumb,tourism.title,b.leader,tourism.diff,tourism.day_num,b.stime,b.leader';
			$where = "tourism.lid in ($lid,0)  and b.state=1 and tourism.tj=1 and (b.stime>'$time' or b.etime>'$time')";
			$result = $this->do->getItems_join($table,$where,$fields,'tourism.sort asc,tourism.id desc',null,null,null,null,'b.tid');
			$news = [];
			$diff = get_diff();
			foreach ($result as $v){
				$v['diff_say'] = $diff[$v['diff']];//难度解析
				$v['stime'] = format_time(strtotime($v['stime']),'m月d日');
				$v['leader'] = json_decode($v['leader'],true)[0];
				if($v['day_num']<5){
					$news['duan'][] = $v;
				}else{
					$news['chang'][] = $v;
				}
			}
			if($result){
				AjaxResult(1,'ok',$news);
			}else{
				AjaxResult_error('没有数据');
			}
		}
	}
	//分类数据
	function cat_lists(){
		if(is_ajax_request()){
			$cid = Posts('cid'); $id = Posts('id');$lid = Posts('lid');
			$time = format_time(time(),'Y-m-d');
			$this->load->model(array('admin/Tourism_model'=>'do'));
			$table = array('ct_tourism_child as b'=>"tourism.id=b.tid+left");
			$fields = 'tourism.id,tourism.thumb,tourism.title,b.leader,tourism.diff,tourism.day_num,b.stime,b.leader';
			$where = "tourism.lid in ($lid,0) and b.state=1 and (b.stime>'$time' or b.etime>'$time')";
			if($cid==1){
				$where .= " and tourism.tgid=$id";
			}elseif($cid==2){
				$where .= " and tourism.diff=$id";
			}elseif($cid==3){
				$where .= $id==5?" and tourism.day_num>=$id":" and tourism.day_num=$id";
			}
			$result = $this->do->getItems_join($table,$where,$fields,'tourism.sort asc,tourism.id desc',null,null,null,null,'b.tid');
			$news = '';
			$diff = get_diff();
			foreach ($result as $v){
				$v['diff_say'] = $diff[$v['diff']];//难度解析
				$v['stime'] = format_time(strtotime($v['stime']),'m月d日');
				$v['leader'] = json_decode($v['leader'],true)[0];
				$news[] = $v;
			}
			if($result){
				AjaxResult(1,'ok',$news);
			}else{
				AjaxResult_error('没有数据');
			}
		}
	}
	
	function tourism_detail(){
		if(is_ajax_request()){
			$id = Posts('id');
			$this->load->model(array('admin/Tourism_model'));
			$data = $this->Tourism_model->getItem(array('id'=>$id),'');//获取主活动
			$diff = get_diff();
			$data['diff_say'] = $diff[$data['diff']];//难度解析
			$data['integral_plus_say'] = $data['integral_plus']?'+'.$data['integral_plus'].' 积分':'';
			AjaxResult(1, '',$data);
		}
	}
	
	//活动详情页面  获取领队 和已经参与的人，以及微信二维码
	function tourism_order(){
		if(is_ajax_request()){
			$cid = Posts('cid','num');
			$this->load->model(array('admin/TourismOrder_model','admin/TourismChild_model','admin/TourismVirtual_model'));
			//报名的人 领队+报名人数+虚拟人数
			$child = $this->TourismChild_model->getItem(array('id'=>$cid),'leader,sign_num,full,qun');
			$leader = json_decode($child['leader'],true);//领队
			$orders_num = $child['sign_num'];//报名总数
			$orders = $this->TourismOrder_model->getItems(array('tcid'=>$cid,'state'=>2),'thumb,uid,nickname,info','',1,5);
			$orders_new = array();
			if($orders){
				foreach ($orders as $v){
					$v['num'] = count(json_decode($v['info'],true));//单个报名总人数
					unset($v['info']);
					$orders_new[] = $v;
				}
			}
			//在后台添加虚拟人数的时候 已经自动增加到报名总数里面了，这里不需计算
			if($orders_num < 5){//是否显示虚拟人数
				$page = 5 - $orders_num;
				$virtual = $this->TourismVirtual_model->getItems(array('tcid'=>$cid),'thumb,nickname,num','',1,$page);
				$o_v = array_merge($orders_new,$virtual);//报名人数和虚拟人数拼合
			}
			$result['sign_num'] = $orders_num;//活动已报名总数
			$result['leader']['user'] = $leader[0];//获取领队第一个人数据
			$result['leader']['num'] = count($leader);//领队总数
			$result['full'] = $child['full'];//活动报名总数
			$result['qun'] = $child['qun'];//活动qun
			$result['order_user'] = get_Nickname(isset($o_v)?$o_v:$orders_new);//如果实际报名人数大于5  则不显示虚拟人数，否则则显示虚拟人数
			if($result['leader']['user']){
				AjaxResult(1,'ok',$result);
			}else{
				AjaxResult_error('没有数据');
			}
		}
	}
	//活动附加产品
	function tgoods_lists(){
		if(is_ajax_request()){
			$tgood = get_Cache('tourism_goods');
			$tg = explode(',', Posts('gid'));
			$tgoods = array();
			foreach ($tgood as $v){
				if(in_array($v['id'], $tg)){
					$v['bangding'] = "bangding".$v['id'];
					$tgoods[] = $v;
				}
			}
			if($tgoods){
				AjaxResult(1,'ok',$tgoods);
			}else{
				AjaxResult_error('没有数据');
			}
		}
	}
	//获取活动 相册数量
	function tourism_photo(){
		if(is_ajax_request()){
			$id = Posts('id');
			$this->load->model(array('admin/Photo_model'));
			$result = $this->Photo_model->count(array('tid'=>$id));
			AjaxResult(1, '',$result);
		}
	}
	
	function location(){
		if(is_ajax_request()){
			$area = get_Cache('location');
			foreach ($area as $v){
				$v['text'] = $v['anames'];
				$result[] = $v;
			}
			if($area){
				AjaxResult(1, '',$result);
			}else{
				AjaxResult_error();
			}
		}
	}
	
	function dingw(){
		if(is_ajax_request()){
			$user = $this->session->wechat_user_session;
			$la = Posts('la');
			$lg = Posts('lg');
			$ipget = file_get_contents("http://api.map.baidu.com/geocoder/v2/?location=$la,$lg&output=json&ak=TFq0f2beXuHEIPK5RgWZlKFr3CIIeaWC");
			$result = json_decode($ipget,true);
			$area = get_Cache('location');
			$l_name = $area[0]['aname'];
			$lid = $area[0]['id'];
			if($result['status']==0){
				$city = $result['result']['addressComponent']['city'];
				foreach ($area as $v){
					if($city == $v['anames']){
						$l_name = $v['aname'];
						$lid = $v['id'];
					}
				}
			}
			$this->load->model('admin/User_model','user');
			$this->user->updates_se(array('location'=>$lid),array('id'=>$user['id']));
			AjaxResult(1, '定位成功',array('name'=>$l_name,'id'=>$lid));
		}
	}
}
