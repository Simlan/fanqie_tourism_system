<?php
defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );
/**
 * @author chaituan@126.com
 */
class Info extends WechatCommon {
	
	function __construct(){
		parent::__construct();
		$this->load->model(array("admin/Info_model"=>"do",'admin/User_model'=>'do_user'));
	}
	
	function lists(){
		if(is_ajax_request()){
			$info = $this->do->getItems(array('uid'=>$this->User['id']));
			if($info){
				foreach ($info as $v){
					$v['state'] = 0;
					if($v['id'] == $this->User['i_id']){
						$v['state'] = 1;
					}
					$items[] = $v;
				}
			}else{
				$items = '';
			}
			AjaxResult(1,'ok', $items);
		}
	}
	
	function add() {
		if(is_ajax_request()){
			$data = Posts('data');
			$data['uid'] = $this->User['id'];
			$state = Posts('state');
			$result = $this->do->add($data);
			if($state){
				$this->do_user->updates_se(array('i_id'=>$result),array('id'=>$this->User['id']));
			}
			is_AjaxResult($result);
		}
	}
	
	function edit(){
		if(is_ajax_request()){
			$data = Posts('data');
			$state = Posts('state');
			$id = Posts('id','num');
			$result = $this->do->updates($data,array('id'=>$id));
			if($state){
				$this->do_user->updates_se(array('i_id'=>$id),array('id'=>$this->User['id']));
			}
			is_AjaxResult($result);
		}
	}
	
	function del(){
		$id = Gets('id','num');
		$state = Gets('state');
		if($state){
			$this->do_user->updates(array('i_id'=>0),array('id'=>$this->User['id']));
		}
		$result = $this->do->deletes(array('id'=>$id));
		is_AjaxResult($result);
	}
}
