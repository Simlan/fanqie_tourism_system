<?php
defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );
/**
 * @author chaituan@126.com
 */
class Pioneer extends WechatCommon {
	function __construct(){
		parent::__construct();
		$this->load->vars('icon_xz',3);
	}
	function index() {
		$this->load->model(array('admin/Ad_model','admin/News_model'));
		//轮播图
		$data['aditems'] = $this->Ad_model->getItems(array('catid'=>4,'state'=>1));
		$this->load->view('mobile/pioneer/index',$data);
	}
	
	function about(){
		$this->load->model(array('admin/News_model'));
		$data['item'] = $this->News_model->getItem(array('id'=>28,'state'=>1),'content,title');
		$this->load->view('mobile/pioneer/about',$data);
	}
	
	function team(){
		$this->load->model(array('admin/News_model'));
		$data['item'] = $this->News_model->getItem(array('id'=>29,'state'=>1),'content,title');
		$this->load->view('mobile/pioneer/team',$data);
	}
	
}
