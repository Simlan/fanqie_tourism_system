<?php
defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );
/**
 * 分类
 * @author chaituan@126.com
 */
class Group extends WechatCommon {
	
	public function index() {
		$data['tag'] = json_encode(get_Cache('tourism_tag'));
		$data['diff'] = json_encode(get_diff());
		$data['day'] = json_encode(array(
				array('name'=>'1天','rate'=>20,'num'=>0,'text'=>1,'id'=>1),
				array('name'=>'2天','rate'=>40,'num'=>0,'text'=>2,'id'=>2),
				array('name'=>'3天','rate'=>60,'num'=>0,'text'=>3,'id'=>3),
				array('name'=>'4天','rate'=>80,'num'=>0,'text'=>4,'id'=>4),
				array('name'=>'5天以上','rate'=>100,'num'=>0,'text'=>'5+','id'=>5)
		));
		$this->load->view('mobile/group/index',$data);
	}
	
	function cat_lists(){
		$data['cid'] = Gets('cid');
		$data['id'] = Gets('id');
		$this->load->view('mobile/group/cat_lists',$data);
	}
	
}
