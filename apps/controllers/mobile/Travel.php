<?php
defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );
/**
 * @author chaituan@126.com
 */
class Travel extends WechatCommon {
	
	function index() {
		$this->load->model(array('admin/Travel_model','admin/TravelCmt_model'));
		$travel = $this->Travel_model->getItems_join(array('travel_photo'=>'travel_photo.tid=travel.id+left'),'','travel.*,travel_photo.thumb','travel.id desc');
		if($travel){
			foreach ($travel as $k=>$v){
				$vs[$v['uid'].$v['addtime']]['items'] = array('star'=>$v['star'],'id'=>$v['id'],'nickname'=>$v['nickname'],'u_thumb'=>$v['u_thumb'],'addtime'=>$v['addtime'],'content'=>$v['content']);
				$vs[$v['uid'].$v['addtime']]['thumb'][] = $v['thumb'];
				$id[$v['id']] = $v['id'];
				$itemNews = $vs;
			}
		}
		
		$id = implode(',', $id);
		$cmt = $this->TravelCmt_model->getItems("tid in ($id)");
		if($cmt){
			foreach ($cmt as $v){
				$newCmt[$v['tid']][] = $v;
			}
		}else{
			$newCmt = '';
		}
		$data['cmt'] = $newCmt;
		$data['items'] = $itemNews;
		$this->load->view('mobile/travel/index',$data);
	}
	
	function upload(){
		$this->load->view('mobile/travel/upload');
	}
	
	function save(){
		if(is_ajax_request()){
			$thumb = array_filter(Posts('thumb'));
			$content = Posts('content');
			$this->load->model(array('admin/Travel_model','admin/TravelPhoto_model'));
			$id = $this->Travel_model->add(array('content'=>$content,'nickname'=>$this->User['nickname'],'uid'=>$this->User['id'],'u_thumb'=>$this->User['thumb'],'addtime'=>time()));
			if($thumb){
				foreach ($thumb as $v){
					$this->TravelPhoto_model->add(array('tid'=>$id,'thumb'=>$v));
				}
			}
			is_AjaxResult($id);
		}
	}
	
	function add(){
		if(is_ajax_request()){
			$path = "/res/upload/images/{yyyy}{mm}{dd}/{time}{rand:6}";
			$config = array ("pathFormat" => $path,"maxSize" => 1000, "allowFiles" => array(".gif",".png",".jpg",".jpeg"),'oriName'=>'123.jpg');
			$this->load->library('Uploader', array ('fileField' =>'thumb','config' => $config,'upload'=>'base64'));
			$info = $this->uploader->getFileInfo();
			if ($info ['state'] == 'SUCCESS') {
				AjaxResult(1, '添加成功',array('thumb'=>$info['url'],'url'=>site_url('mobile/travel/del')));
			} else {
				AjaxResult_error($info['state']);	
			}
		}
	}
	
	function del(){
		if(is_ajax_request()){
			$thumbs = substr(Posts('thumb'),1);
			if (is_file($thumbs)) {
				if (unlink($thumbs)) {
					AjaxResult_ok('删除成功');
				} else {
					AjaxResult_error('删除失败');
				}
			} else {
				AjaxResult_error('操作失败,文件路径出问题');
			}
		}
	}
	
	function star(){
		$id = Gets('id');
		$this->load->model(array('admin/Travel_model','admin/Star_model'));
		$item = $this->Star_model->getItem(array('uid'=>$this->User['id'],'tid'=>$id));
		if($item)AjaxResult_error('您已经点过赞了');
		$this->Travel_model->updates(array('star'=>'+=1'),array('id'=>$id));
		$result = $this->Star_model->add(array('uid'=>$this->User['id'],'tid'=>$id));
		is_AjaxResult($result,'点赞成功');
	}
	
	function cmt(){
		if(is_ajax_request()){
			$id = Posts('id');$say = Posts('say');
			$this->load->model('admin/TravelCmt_model');
			$data = array('tid'=>$id,'uid'=>$this->User['id'],'nickname'=>$this->User['nickname'],'thumb'=>$this->User['thumb'],'says'=>$say,'addtime'=>time());
			$result = $this->TravelCmt_model->add($data);
			is_AjaxResult($result);
		}
	}
	
	function get_cmt(){
		$id = Gets('id');
		$this->load->model('admin/TravelCmt_model');
		$items = $this->TravelCmt_model->getItems(array('tid'=>$id));
		if($items){
			AjaxResult(1, '',$items);
		}else{
			AjaxResult_error();
		}
	}
	
}
